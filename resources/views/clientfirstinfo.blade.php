<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Protezione Famiglia</title>
        <link href="home/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="home/css/font-awesome.min.css">
        <link rel="stylesheet" href="home/css/animate.css">
        <link rel="stylesheet" href="home/css/overwrite.css">
        <link href="home/css/animate.min.css" rel="stylesheet">
        <link href="home/css/style.css" rel="stylesheet">
        <link href="css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">
        
        <header id="header">
            <nav class="navbar navbar-fixed-top" role="banner">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        </button> 

                        <a class="navbar-brand" href="/"><img src="img/fep3Picture1.png" height="100" width="160"></a>

                        <!--<a class="navbar-brand" href="/"><img src="img/fep3Picture1.png" height="100" width="200"></a>-->

                    </div>
                 
                    
                    <div class="navbar-image">
                        <img src="img/logo_for_bridget.png" align ="right" alt=""/>
                   <!-- <img src="img/fep3Picture1.png" height="100" width="200" align ="right">-->
                   </div>
                    
                       
                    <div class="navbar-image">
                       <div class="row">
                        <div class="col-md-6">
                        <h1 class = "text-center" style= "color: #ff7302;"> Sai di che assicurazione <br>
                            hai realmente bisogno?</h1>
                        <!--<img src="img/textimagePicture1.png"  height= "120" width= "550" align ="center" alt=""/> -->
                   <!-- <img src="img/fep3Picture1.png" height="100" width="200" align ="right">-->
                
                
                    </div>
                     </div>
                     </div>
                     
                   <!-- <div class="collapse navbar-collapse navbar-right">
                        <ul class="nav navbar-nav">
                            @if (! Auth::check())
                            <li><a href="{{ route('register') }}">Registrati</a></li>
                            <li><a href="{{ route('login') }}">Login</a></li>
                            @else
                            <li><a>Welcome {{ Auth::user()->name }}</a></li>
                            <li><a href="{{ route('signout') }}">Esci</a></li>
                            @endif
                        </ul>
                    </div>-->
                </div><!--/.container-->
            </nav><!--/nav-->
        </header><!--/header-->
                    
    </head>
<body>
<div style="margin-top: 25px;" id="feature">
    <div class="container">
        <div class="row">

            <hr style="min-width:85%; background-color:#2f4f4f !important; height:6px;"/>

            <!--<hr style="min-width:85%; background-color:#2f4f4f !important; height:8px;"/>-->

            <p>
                @if (session('clientfirstinfo'))
                    <div class="alert alert-danger">
                        {{ session('clientfirstinfo') }}!
                    </div>
                @endif
            </p>
            <div class="col-xs-12 col-md-6">
                {{-- <p class="text-muted"><small>PASSAGGIO 1 DI 7 </small></p> --}}
                <p class="text-muted"><small>PASSAGGIO PRELIMINARE </small></p>
                <p>Registrati per aprire il tuo account</p>
                <div class="navbar-image">

                    <img src="img/Picturepage1.png" align ="left" width="450" height ="420">

                    <!--<img src="img/Picturepage1.png" align ="left">-->

                </div>
            </div>
            <div class="col-xs-6 col-md-6">
                <p>Qualche dato</p>
                <form class="form-horizontal" role="form" method="POST" action="{{ route('postClientFirstInfo') }}">
                    {{ csrf_field() }}
                    <div class="form-group{{ $errors->has('fName') ? ' has-error' : '' }}">
                        <label for="name" class="col-md-4">Nome</label>
                        <div class="col-md-8">
                            <input id="name" type="text" class="form-control" name="fName" value="{{ old('fName') }}">
                            @if ($errors->has('fName'))
                            <span class="help-block">
                                <strong>{{ $errors->first('fName') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('surname') ? ' has-error' : '' }}">
                        <label for="cognome" class="col-md-4">Cognome</label>
                        <div class="col-md-8">
                            <input id="cognome" type="text" class="form-control" name="surname" value="{{ old('surname') }}">
                            @if ($errors->has('surname'))
                            <span class="help-block">
                                <strong>{{ $errors->first('surname') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('date') ? ' has-error' : '' }}">
                        <label for="date" class="col-md-4">Data di nascita</label>
                        <div class="col-md-8">
                            <input id="date" type="date" class="form-control" name="date" value="{{ old('date') }}">
                            @if ($errors->has('date'))
                            <span class="help-block">
                                <strong>{{ $errors->first('date') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('gender') ? ' has-error' : '' }}">
                        <div class="col-md-8">
                            <b>Maschio &nbsp;  &nbsp;</b> <input type="radio" name="gender" value="male"> </input>
                            <b> &nbsp Femmina &nbsp</b> <input type="radio" name="gender" value="female"></input>
                            @if ($errors->has('gender'))
                            <span class="help-block">
                                <strong>{{ $errors->first('gender') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('mail') ? ' has-error' : '' }}">
                        <label for="mail" class="col-md-4">Mail</label>
                        
                        <div class="col-md-8">
                            <input id="mail" type="text" class="form-control" name="mail" value="{{ old('mail') }}">
                            @if ($errors->has('mail'))
                            <span class="help-block">
                                <strong>{{ $errors->first('mail') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    
                    <div class="form-group{{ $errors->has('telephone') ? ' has-error' : '' }}">
                        <label for="mail" class="col-md-4">Telefone</label>
                        
                        <div class="col-md-8">
                            <input id="mail" type="text" class="form-control" name="telephone" value="{{ old('telephone') }}">
                            @if ($errors->has('telephone'))
                            <span class="help-block">
                                <strong>{{ $errors->first('telephone') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class=" col-xs-6 col-md-8">
                        
                    </div> 
                    <div class="col-md-8">

                        <div class="form-group">
                        <label> Accetazione Privacy*</label><br>
                        <textarea class="textarea"> Ai sensi dell'art. 13 del d.lgs. 196/2003, Codice in materia di protezione dei dati personali, Comunica srl. (di seguito anche "Società", e/o " Comunica srl. "),con sede in Via Roberto Ardigò 13/B.</textarea>
                        
                            <div class="accetto form-group{{ $errors->has('accept') ? ' has-error' : '' }}"><label>
                                <input type="checkbox" name = "accept" id ="accetto"><i></i>&nbsp; Accetto
                            </label>
                            @if ($errors->has('accept'))
                            <span class="help-block">
                                <strong>{{ $errors->first('accept') }}</strong>
                            </span>
                            @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8">
                        
                    </div>

                    <div class="col-md-8">
                        <a href="{{URL::previous()}}"type="submit" style="height:40px; width:140px; background-color:#2f4f4f"  class="btn btn-primary block full-width m-b">Indietro</a>
                    </div>
                    <div class="col-xs-6 col-md-4">
                        <button type="submit" style="height:40px; width:140px" class="btn btn-primary block full-width m-b">Registrati</button>
                </div>
            </div>
            </form>
           


        </div>
         <hr style="min-width:85%; background-color:#f09300!important; height:6px;"/>
    </div>
    </body>
</html>