     <!DOCTYPE html>
    <html>
      <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Protezione Famiglia</title>
        <link href="home/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="home/css/font-awesome.min.css">
        <link rel="stylesheet" href="home/css/animate.css">
        <link rel="stylesheet" href="home/css/overwrite.css">
        <link href="home/css/animate.min.css" rel="stylesheet"> 
        <link href="home/css/style.css" rel="stylesheet"> 
        <link href="css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">
  
        <header id="header">
        <nav class="navbar navbar-fixed-top" role="banner">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <a class="navbar-brand" href="/"><img src="img/fep3Picture1.png" height="100" width="160"></a>
                        </div> 
                        <div class="navbar-image">
                        <img src="img/logo_for_bridget.png" align ="right" alt=""/>
                   
                </div>
                  
                       <div class="row">
                        <div class="col-md-8">
                        <h1 class = "text-center" style= "color: #ff7302;" style ="font-size:320px;"> Protezione Famiglia<br>
                          </h1>
                        </div> 
                        <div class="navbar-image">
                        
                        </div>
                        
                        @if (! Auth::check())
                        <li><a href="{{ route('register') }}">Registrati</a></li>
                        <li><a href="{{ route('login') }}">Login</a></li>  
                        @else
                        <li><a>Bevenuto {{ Auth::user()->name }}</a></li>
                        <li><a href="{{ route('signout') }}">Esci</a></li>  
                        @endif                
                    </ul>
                </div>. -->

            </div><!--/.container-->
        </nav><!--/nav-->       
    </header><!--/header--> 
    

</head>
    <body>

        <div style="margin-top: 25px;" id="feature">
            <div class="container">
            {{-- <div class="row"> 
        

        <p>PASSAGGIO 1 DI 5 </p><br>
         <p>Cerchiamo di capire la tua <br>
            situazione generale</p>

    
</div> --}}

<div class="row">

    <hr style="min-width:85%; background-color:#2f4f4f !important; height:6px;"/>
    
    <p>
        @if (session('clientinfo'))
            <div class="alert alert-danger">
                {{ session('clientinfo') }}!
            </div>
        @endif
    </p>
  <div class="col-xs-12 col-md-6">
    

    <!--<hr style="min-width:85%; background-color:#2f4f4f !important; height:10px;"/>
    <p class="text-muted"><small>PASSAGGIO 1 DI 7 </small></p>-->
    
  <div class="col-xs-12 col-md-6">

<p class="text-muted"><small>PASSAGGIO 1 DI 7 </small></p>
    <p>Cerchiamo di capire la tua <br>
        situazione generale</p>
    <div class="navbar-image">

      <img src="img/Picturepage1.png"  width = "470" height ="400"align ="left">     

    </div>
  </div>
  </div>
  <div class="col-xs-6 col-md-6">
    
    <form class="form-horizontal" role="form" method="POST" action="{{ route('postClientinfo') }}">
        {{ csrf_field() }}
        <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
            <p class="col-md-12" >Qualche dato</p>
            <label for="name" class="col-md-4">Nome</label>

            <div class="col-md-8">
                <input id="name" type="text" class="form-control" name="first_name" value="{{ old('first_name') }}">

                @if ($errors->has('first_name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('first_name') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('surname') ? ' has-error' : '' }}">
            <label for="cognome" class="col-md-4">Cognome</label>

            <div class="col-md-8">
                <input id="cognome" type="text" class="form-control" name="surname" value="{{ old('surname') }}">

                @if ($errors->has('surname'))
                    <span class="help-block">
                        <strong>{{ $errors->first('surname') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('date') ? ' has-error' : '' }}">
            <label for="date" class="col-md-4">Data di nascita</label>

            <div class="col-md-8">
                <input id="date" type="date" class="form-control" name="date" value="{{ old('date') }}">

                @if ($errors->has('date'))
                    <span class="help-block">
                        <strong>{{ $errors->first('date') }}</strong>
                    </span>
                @endif
            </div>
        </div>


        <div class="form-group{{ $errors->has('place_of_birth') ? ' has-error' : '' }}">
            <label for="place_of_birth" class="col-md-4">Nata a</label>

            <div class="col-md-8">
                <input id="nata a" type="text" class="form-control" name="place_of_birth" value="{{ old('place_of_birth') }}">

                @if ($errors->has('place_of_birth'))
                    <span class="help-block">
                        <strong>{{ $errors->first('place_of_birth') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
            <label for="address" class="col-md-4">Residente</label>

            <div class="col-md-8">
                <input id="residenza" type="text" class="form-control" name="address" value="{{ old('address') }}">

                @if ($errors->has('address'))
                    <span class="help-block">
                        <strong>{{ $errors->first('address') }}</strong>
                    </span>
                @endif
            </div>
        </div>




        <div class="form-group{{ $errors->has('gender') ? ' has-error' : '' }}">
            <!--<div class="col-md-9">-->

            <div class="col-md-8">
                 <input type="radio" name="gender" value="male"> &nbsp; <b>Maschio &nbsp; &nbsp;</b>
                 <input type="radio" name="gender" value="female">  <b>  &nbsp; Femmina</b>
            </div>
            @if ($errors->has('gender'))
                <span class="help-block">
                    <strong>{{ $errors->first('gender') }}</strong>
                </span>
            @endif
        </div>

        <div class="ihr-line-dashed">
        </div>

   <!-- <div class ="form-group">
        <label class ="col-md-8 control-label"> </label>
            <p class="col-md-12"><b>In quale situazione ti trovi?</b></p>
    <div class="col-md-8">
        <div class =" i-checks">
            <label> 
            
            <input type="radio" name="bridgatio">&nbsp;&nbsp;Lavoro</label>
-->
        <div class ="form-group{{ $errors->has('work_status') ? ' has-error' : '' }}">
        <label class ="col-md-8 control-label"> </label>
            <p class="col-md-12"><b>In quale situazone ti trovi?</b></p>
            <div class="col-md-8">
            <div class =" i-checks">
            <label> 
            
            <input type="radio" style ="vertical-align: middle"name="work_status" value="lavoro"> &nbsp; lavoro </label>

            <i></i> 
        </div>
        <div class ="i-checks">
            <label> 

            <input type="radio"  name="work_status" value="occupazione">&nbsp;&nbsp;Occupazione</label>
            <i></i> 
        </div>
        <div class ="i-checks">
            <label> 

            <input type="radio" name="work_status" value="pensionato">&nbsp;&nbsp;Pensionato</label>


            <i></i> 
        </div>
        <div class ="i-checks">
            <label> 

            <input type="radio" name="work_status" value="studente">&nbsp;&nbsp;Studente  </label>
            <i></i> 
        </div>
        </div>
        @if ($errors->has('work_status'))
            <span class="help-block">
                <strong>{{ $errors->first('work_status') }}</strong>
            </span>
        @endif
    </div>
    <div class="form-group{{ $errors->has('employment_status') ? ' has-error' : '' }}">
        <div class="col-md-12">
             <input type="radio" name="employment_status" value="self employed"> &nbsp; <b>Sei un lavoratore autonomo?&nbsp;</b>
             <input type="radio" name="employment_status" value="salary earner">&nbsp;  <b>Sei un lavoratore dipendente?&nbsp; </b>
        </div>
        @if ($errors->has('employment_status'))
            <span class="help-block">
                <strong>{{ $errors->first('employment_status') }}</strong>
            </span>
        @endif
    </div>    
        <!--</div>-->

        <!--</div>-->
        

        <div class="form-group{{ $errors->has('business_type') ? ' has-error' : '' }}">
            <label for="quale" class="col-md-4">Quale e la tua attivita?</label>

            
                <select style="margin-left: 4%;" class ="col-md-7" name="business_type">
                <optgroup label ="Addetto negozio o esercizio commerciale">
                  <option value="">Select...</option>
                  <option value="abiti">abiti, confezioni</option>
                  <option value="saab">accessori per autoveicoli,</option>
                  <option value="mercedes">armeria con confezionamento di cartucce e munizione</option>
                  <option value="articoli1">articoli ed apparecchi fotografici e di ottica</option>
                  <option value="articoli2">articoli igienico-sanitari</option>
                  <option value="articoli3">articoli in pelle, calzature</option>
                  <option value="audi5">articoli sportivi</option>
                  <option value="audi6">bar, caffè, birreria</option>
                  <option value="audi7">cartoleria, libreria</option>
                  <option value="audi8">acasalinghi</option>
                  <option value="audi9">colori e vernici</option>
                  <option value="audi10">computers, mobili per ufficio</option>
                  <option value="audi11">dischi, musica, strumenti musicali</option>
                  <option value="audi12">drogheria, vini e liquori</option>
                <option value="audi13">elettrodomestici, radio, TV (esclusa posa di antenne)</option>
                <option value="audi14">drogheria, vini e liquori</option>
                <option value="audi15">ferramenta</option>
                <option value="audi16">fiori e piante</option>
                <option value="audi17">frutta e verdura</option>
                <option value="audi18">generi alimentari</option>
                <option value="audi19"> di giocattoli</option>
                <option value="audi20">macelleria (esclusa macellazione)</option>
                <option value="audi21">merceria, tessuti</option>
                <option value="audi22">mobili</option>
                <option value="audi23">oreficeria, orologeria, gioielleria</option>
                <option value="audi24">panetteria, latteria, pasticceria, gelateria: senza produzione</option>
                <option value="audi25">panetteria, latteria, pasticceria, gelateria: con produzione</option>
                <option value="audi26">pellicce, capi in pelle</option>
                <option value="audi27">pescheria</option>
                <option value="audi28">profumeria</option>
                <option value="audi29">ristorante, trattoria, pizzeria</option>
                <option value="audi30">salumeria, rosticceria</option>
                <option value="audi31">surgelati</option>
                <option value="audi32">tabaccheria</option>
                <option value="audi33">tintoria, lavanderia</option>
                </optgroup>

                <option value="audi34">Addetto pompe funebri</option>
                <option value="audi35">Agente di assicurazione</option>
                <option value="audi36">Agente di borsa</option>
                <option value="audi37">Agente di cambio</option>
                <option value="audi38">Agente di commercio</option>
                <option value="audi39">Agente di custodia</option>
                <option value="audi40">Agente di pubblicità</option>
                <option value="audi41">Agente di polizia di stato</option>
                <option value="audi42">Agente Polizia ferroviaria</option>
                <option value="audi43">Agente immobiliare</option>
                <option value="audi44">Agricoltore (proprietario, affittuario, ecc.) con mansioni amministrative</option>
                <option value="audi45">Agricoltore con prestazioni manuali con guida di macchine agricole</option>
                <option value="audi46">Agricoltore con prestazioni manuali senza guida di macchine agricole</option>
                <option value="audi47">Agronomo</option>
                <option value="audi48">Albergatore con prestazioni manuali</option>
                <option value="audi49">Albergatore senza prestazioni manuali</option>
                </select>

                @if ($errors->has('business_type'))
                    <span class="help-block">
                        <strong>{{ $errors->first('business_type') }}</strong>
                    </span>
                @endif
        </div>
        <div class="col-md-8">
                        
        </div>
        <div class="col-md-8">
           <a href="{{URL::previous()}}"type="submit" style="height:40px; width:140px; background-color:#2f4f4f"  class="btn btn-primary block full-width m-b">Indietro</a>
        </div>
        <div class="col-xs-6 col-md-4">
            <button type="submit" style="height:40px; width:140px"class="btn btn-primary block full-width m-b">Avanti</button>
        </div>

  </div>
  
</div>
    </form>
    <hr style="min-width:85%; background-color:#f09300!important; height:6px;"/>

  </div>
  
</div>

</div>
</div>
</body>
</html>
