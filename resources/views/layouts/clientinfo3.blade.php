    <!DOCTYPE html>
    <html>
      <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Protezione Famiglia</title>
        <link  href="{!! asset('home/css/bootstrap.min.css') !!}" rel="stylesheet">
        <link rel="stylesheet" href="{!! asset('home/css/font-awesome.min.css') !!}">
        <link rel="stylesheet" href="{!! asset('home/css/animate.css') !!}">
        <link rel="stylesheet" href="{!! asset('home/css/overwrite.css') !!}">
        <link href="{!! asset('home/css/animate.min.css') !!}" rel="stylesheet"> 
        <link href="{!! asset('home/css/style.css') !!}" rel="stylesheet"> 

        <link href="{!! asset('home/css/selectoption.css') !!}" rel="stylesheet"> 
        <link href="{!! asset('css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css') !!}"  rel="stylesheet">
  
  

        <link href="{!! asset('css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css') !!}"  rel="stylesheet">
  

        <header id="header">
        <nav class="navbar navbar-fixed-top" role="banner">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>


                    <a class="navbar-brand" href="/"><img src="{!! asset ('img/fep3Picture1.png')!!}" align = "right" height="100" width="160"/></a>
                </div>
 
                 <div class="navbar-image">
                            
            
                        <div class="col-md-8">
                        <div class="col-md-12">
                        <h1 class = "text-center" style= "color: #ff7302;" style ="font-size:320px;"> Protezione Famiglia<br>
                          </h1>

                         </div>
                          </div>
                          </div> 
                <div class="navbar-image">
                 <div class="col-md-2">
                 <a class="navbar-brand" href="/"><img src="{!! asset('img/logo_for_bridget.png') !!}"  align ="left" alt=""/></a>
                 
                   <!-- <img src="img/fep3Picture1.png" height="100" width="200" align ="right">-->
                </div>
                </div>











                   <!-- <a class="navbar-brand" href="/"><img src="{!! asset('img/logo_for_bridget.png') !!}" alt=""/></a>-->
                </div>              

                <!--<div class="collapse navbar-collapse navbar-right">
                    <ul class="nav navbar-nav">
                        
                        @if (! Auth::check())
                        <li><a href="{{ route('register') }}">Registrati</a></li>
                        <li><a href="{{ route('login') }}">Login</a></li>  
                        @else
                        <li><a>Bevenuto {{ Auth::user()->name }}</a></li>
                        <li><a href="{{ route('signout') }}">Esci</a></li>  
                        @endif                
                    </ul>
                </div>
   -->
            </div> <!--/.container-->
        </nav><!--/nav-->       
    </header><!--/header--> 
    

</head>
    <body>

        <div style="margin-top: 25px;" id="feature">
            <div class="container">

<div class="row">

   <hr style="min-width:85%; background-color:#2f4f4f !important; height:6px;"/>

   <p>
        @if (session('clientinfopage2'))
            <div class="alert alert-success">
                {{ session('clientinfopage2') }}!
            </div>
        @endif
    </p>
    <p>
        @if (session('clientinfopage3'))
            <div class="alert alert-danger">
                {{ session('clientinfopage3') }}!
            </div>
        @endif
    </p>
    
  <div class="col-xs-12 col-md-6">
<p class="text-muted"><small>PASSAGGIO 3 DI 7 </small></p>
    
    <p><b>Parliamo un pò di te</b></p>
    <div class="navbar-image">

      <img src="{!! asset('img/Picturepage3.png') !!}" height = "400" width= "400" align ="left">   

      

    </div>
  </div>
  <div class="col-xs-6 col-md-6">
    <p><b>Quale il tuo stato civile?</b></p>
    <form class="form-horizontal" role="form" method="POST" action="{{ route('postClientInfoPage3') }}">
        {{ csrf_field() }}
        <div class="form-group{{ $errors->has('marital_status') ? ' has-error' : '' }}">

            <div class="col-md-8">
                <input type="radio" name="marital_status" value="lavaro">&nbsp;&nbsp;Single<br>
                <input type="radio" name="marital_status" value="occupation">&nbsp;&nbsp;Sposato<br>
                <input type="radio" name="marital_status" value="pensionato">&nbsp;&nbsp;Convivente<br>
                 @if ($errors->has('marital_status'))
                    <span class="help-block">
                        <strong>{{ $errors->first('marital_status') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('spouse_activity') ? ' has-error' : '' }}">
            <p class="col-md-6"><b>Quale è l'attività del coniuge?</b></p>
            <div class="col-md-6">
                <select class ="col-md-10" name="spouse_activity">
                <optgroup label ="Addetto negozio o esercizio commerciale">
                  <option value="">Select...</option>
                  <option value="abiti">abiti, confezioni</option>
                  <option value="saab">accessori per autoveicoli,</option>
                  <option value="mercedes">armeria con confezionamento di cartucce e munizione</option>
                  <option value="articoli1">articoli ed apparecchi fotografici e di ottica</option>
                  <option value="articoli2">articoli igienico-sanitari</option>
                  <option value="articoli3">articoli in pelle, calzature</option>
                  <option value="audi5">articoli sportivi</option>
                  <option value="audi6">bar, caffè, birreria</option>
                  <option value="audi7">cartoleria, libreria</option>
                  <option value="audi8">acasalinghi</option>
                  <option value="audi9">colori e vernici</option>
                  <option value="audi10">computers, mobili per ufficio</option>
                  <option value="audi11">dischi, musica, strumenti musicali</option>
                  <option value="audi12">drogheria, vini e liquori</option>
                <option value="audi13">elettrodomestici, radio, TV (esclusa posa di antenne)</option>
                <option value="audi14">drogheria, vini e liquori</option>
                <option value="audi15">ferramenta</option>
                <option value="audi16">fiori e piante</option>
                <option value="audi17">frutta e verdura</option>
                <option value="audi18">generi alimentari</option>
                <option value="audi19"> di giocattoli</option>
                <option value="audi20">macelleria (esclusa macellazione)</option>
                <option value="audi21">merceria, tessuti</option>
                <option value="audi22">mobili</option>
                <option value="audi23">oreficeria, orologeria, gioielleria</option>
                <option value="audi24">panetteria, latteria, pasticceria, gelateria: senza produzione</option>
                <option value="audi25">panetteria, latteria, pasticceria, gelateria: con produzione</option>
                <option value="audi26">pellicce, capi in pelle</option>
                <option value="audi27">pescheria</option>
                <option value="audi28">profumeria</option>
                <option value="audi29">ristorante, trattoria, pizzeria</option>
                <option value="audi30">salumeria, rosticceria</option>
                <option value="audi31">surgelati</option>
                <option value="audi32">tabaccheria</option>
                <option value="audi33">tintoria, lavanderia</option>
                </optgroup>

                <option value="audi34">Addetto pompe funebri</option>
                <option value="audi35">Agente di assicurazione</option>
                <option value="audi36">Agente di borsa</option>
                <option value="audi37">Agente di cambio</option>
                <option value="audi38">Agente di commercio</option>
                <option value="audi39">Agente di custodia</option>
                <option value="audi40">Agente di pubblicità</option>
                <option value="audi41">Agente di polizia di stato</option>
                <option value="audi42">Agente Polizia ferroviaria</option>
                <option value="audi43">Agente immobiliare</option>
                <option value="audi44">Agricoltore (proprietario, affittuario, ecc.) con mansioni amministrative</option>
                <option value="audi45">Agricoltore con prestazioni manuali con guida di macchine agricole</option>
                <option value="audi46">Agricoltore con prestazioni manuali senza guida di macchine agricole</option>
                <option value="audi47">Agronomo</option>
                <option value="audi48">Albergatore con prestazioni manuali</option>
                <option value="audi49">Albergatore senza prestazioni manuali</option>
                </select>

                @if ($errors->has('spouse_activity'))
                    <span class="help-block">
                        <strong>{{ $errors->first('spouse_activity') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('residential') ? ' has-error' : '' }}">
            <p class="col-md-8"><b>Dove vivi?</b></p>
            <div class="col-md-12">

                <input type="radio" name="residential" value="casa genitori">&nbsp;&nbsp;In casa dei tuoi genitori 
                 
            </div>
            <div class="col-md-8">
                <input type="radio" name="residential"value="casa proprietà">&nbsp;&nbsp;In casa di proprietà
            
            </div>
            <br>
            <div class="col-md-8">
              <input type="radio" name="residential" value="in affitto">&nbsp;&nbsp;In affito &#160;
            </div>
            <div class="col-md-7">
               <p>Dati Immobile 
                <a href="{{ route('datamobili') }}" style="margin-top: 1%;" type="submit" class="btn btn-primary btn-m-b">Apri</a></p>     
            </div>
            @if ($errors->has('residential'))
                <span class="help-block">
                    <strong>{{ $errors->first('residential') }}</strong>
                </span>
            @endif
        </div>
        <div class="form-group{{ $errors->has('has_property_for_rent') ? ' has-error' : '' }}">

            <p class="col-md-12">
                <p><b>Altro</b></p>
                Posseidi proprieta che affiti? o per investimento?</p>

            <div class="col-md-8">
                <input type="radio" name="has_property_for_rent" value="affitosi">&nbsp;&nbsp;Si &nbsp;&nbsp;
                <input type="radio" name="has_property_for_rent" value="affittono">&nbsp;&nbsp;No
            </div>
            @if ($errors->has('has_property_for_rent'))
                <span class="help-block">
                    <strong>{{ $errors->first('has_property_for_rent') }}</strong>
                </span>
            @endif
        </div>
      
<div class="col-md-8">
                        
</div>
<div class="col-md-8">
        <a href="{{URL::previous()}}"type="submit" style="height:40px; width:140px; background-color:#2f4f4f"  class="btn btn-primary block full-width m-b">Indietro</a>
        </div>
<div class="col-md-4">
 <button style="height:40px; width:140px" type="submit" class="btn btn-primary block full-width m-b">Avanti</button>
         </div>
    </form>
  </div>
</div>
<hr style="min-width:85%; background-color:#f09300 !important; height:6px;"/>
</div>
</div>
    </form>
  </div>
</div>
</div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" src="{!! asset('home/js/bootstrap.min.js') !!}"></script>
<script>
  $(document).ready(function() {  
    $('input[name="optradio1"]').change(function() {
        $('#myModal').modal('show');
    });

    $('input[name="optradio2"]').change(function() {
        $('#myModal2').modal('show');
    });
  });
</script>
</body>
</html>