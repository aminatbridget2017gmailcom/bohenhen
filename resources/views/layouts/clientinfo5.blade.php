    <!DOCTYPE html>
    <html>
      <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Protezione Famiglia</title>
        <link  href="{!! asset('home/css/bootstrap.min.css') !!}" rel="stylesheet">
        <link rel="stylesheet" href="{!! asset('home/css/font-awesome.min.css') !!}">
        <link rel="stylesheet" href="{!! asset('home/css/animate.css') !!}">
        <link rel="stylesheet" href="{!! asset('home/css/overwrite.css') !!}">
        <link href="{!! asset('home/css/animate.min.css') !!}" rel="stylesheet"> 
        <link href="{!! asset('home/css/style.css') !!}" rel="stylesheet"> 
        <link href="{!! asset('css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css') !!}"  rel="stylesheet">
        <link rel="stylesheet" href="{!! asset('home/ion.rangeSlider/css/ion.rangeSlider.css') !!}">
        <link rel="stylesheet" href="{!! asset('home/ion.rangeSlider/css/ion.rangeSlider.skinFlat.css') !!}">
        <link  href="{!! asset('home/noUiSlider.11.1.0/nouislider.min.css') !!}" rel="stylesheet">
  
        <header id="header">
        <nav class="navbar navbar-fixed-top" role="banner">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                   <a class="navbar-brand" href="/"><img src="{!! asset ('img/fep3Picture1.png')!!}" align = "right" height="100" width="160"/></a>
                </div>
 
                 <div class="navbar-image">
                            
            
                        <div class="col-md-8">
                        <div class="col-md-12">
                        <h1 class = "text-center" style= "color: #ff7302;" style ="font-size:320px;"> Protezione Famiglia<br>
                          </h1>

                         </div>
                          </div>
                          </div> 
                <div class="navbar-image">
                 <div class="col-md-2">
                 <a class="navbar-brand" href="/"><img src="{!! asset('img/logo_for_bridget.png') !!}"  align ="left" alt=""/></a>
                 
                   <!-- <img src="img/fep3Picture1.png" height="100" width="200" align ="right">-->
                </div>
                </div>







                <!--<div class="collapse navbar-collapse navbar-right">
=======
                    <a class="navbar-brand" href="/"><img src="{!! asset('img/logo_for_bridget.png') !!}" alt=""/></a>
                </div>              
                <div class="collapse navbar-collapse navbar-right">
>>>>>>> be4a52f2487bc0395c7b664b9e222fa9363f7a00
                    <ul class="nav navbar-nav">
                        
                        @if (! Auth::check())
                        <li><a href="{{ route('register') }}">Registrati</a></li>
                        <li><a href="{{ route('login') }}">Login</a></li>  
                        @else
                        <li><a>Welcome {{ Auth::user()->name }}</a></li>
                        <li><a href="{{ route('signout') }}">Esci</a></li>  
                        @endif                
                    </ul>
<<<<<<< HEAD
                </div> -->

                </div>

            </div><!--/.container-->
        </nav><!--/nav-->       
    </header><!--/header--> 
    

</head>
    <body>

        <div style="margin-top: 25px;" id="feature">
            <div class="container">

<div class="row">
    

     <hr style="min-width:85%; background-color:#2f4f4f !important; height:6px;"/>

     

  <div class="col-xs-12 col-md-6">
   <p class="text-muted"><small>PASSAGGIO 5 DI 7 </small></p>
    <p>Cerchiamo di conoscere la tua <br>
        situazione finanziaria</p>
    <div class="navbar-image">
      <img src="{!! asset('img/fep2Picture1.png') !!}" height = "400" width= "400" align ="left">   
    </div>

    <!-- <div class="row">
        <p><b>Perche ti facciamo queste domande?</b></p>
        <p>
            La maggior parte dellepersonal utilizza i proppiredditi
        </p>
    </div> -->
  </div>
  <div class="col-xs-6 col-md-6">
    <!-- <p>Qual e il tuo stato civile?</p> -->
    <form class="form-inline" role="form" method="POST">
        {{ csrf_field() }}
       <p>Inserisci l'ammontare totale per ogni voce</p>
       <div class="form-group{{ $errors->has('mutual1') ? ' has-error' : '' }}">
            <div class="row">
                <div class="col-md-4">
                    <select name="mutual1">
                      <option value="volvo">Mutuo</option>
                      <option value="saab">Prestito</option>
                      <option value="mercedes">Leasing</option>
                      <option value="audi">Fido</option>

                      <option value="carta-credito">Carta di credito</option>
                      <option value="altro">Altro</option>


                    </select>

                    @if ($errors->has('mutual1'))
                        <span class="help-block">
                            <strong>{{ $errors->first('mutual1') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group{{ $errors->has('mutual2') ? ' has-error' : '' }}">
                <div class="col-md-4">
                    <select name="mutual2">
                      <option value="volvo">Mutuo</option>
                      <option value="saab">Prestito</option>
                      <option value="mercedes">Leasing</option>
                      <option value="audi">Fido</option>

                      <option value="carta-credito">Carta di credito</option>
                      <option value="altro">Altro</option>


                    </select>

                    @if ($errors->has('mutual2'))
                        <span class="help-block">
                            <strong>{{ $errors->first('mutual2') }}</strong>
                        </span>
                    @endif
                </div>
                </div>
                <div class="col-md-4">
                <div class="form-group{{ $errors->has('mutual3') ? ' has-error' : '' }}">
                    <select name="mutual3">
                      <option value="volvo">Mutuo</option>
                      <option value="saab">Prestito</option>
                      <option value="mercedes">Leasing</option>
                      <option value="audi">Fido</option>

                      <option value="carta-credito">Carta di credito</option>
                      <option value="altro">Altro</option>


                    </select>

                    @if ($errors->has('mutual3'))
                        <span class="help-block">
                            <strong>{{ $errors->first('mutual3') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            </div><br>

            <div style="margin-top: 10px;" class="row">
                <div class="col-md-4">
                     <label> Rata mensile </label><br>
                     <div id="number-range"></div>
                      <div id="basic_slider"></div>
                </div>
                <div class="col-md-4">
                     <label> Rata mensile </label><br>
                    <div id="number-range2"></div>
                    <div id="basic_slider2"></div>
                </div>
                <div class="col-md-4">
                     <label> Rata mensile </label><br>
                    <div id="number-range3"></div>
                    <div id="basic_slider3"></div>
                </div>

            </div>

            <div style="margin-top: 10px;" class="row">
                <div class="col-md-4">
                    <label> Importo residuo </label><br>
                    <div id="number-range4"></div>
                    <div id="basic_slider4"></div>
                </div>
                <div class="col-md-4">
                     <label> Importo residuo </label><br>
                    <div id="number-range5"></div>
                    <div id="basic_slider5"></div>
                </div>
                <div class="col-md-4">
                     <label> Importo residuo </label><br>
                    <div id="number-range6"></div>
                    <div id="basic_slider6"></div>
                </div>
            </div>

            <div style="margin-top: 10px;" class="row">
                <div class="col-md-4">
                     <label> Residuo anni </label><br>
                    <div id="number-range7"></div>
                    <div id="basic_slider7"></div>
                </div>
                <div class="col-md-4">
                    <label> Residuo anni </label><br>
                    <div id="number-range8"></div>
                    <div id="basic_slider8"></div>
                </div>
                <div class="col-md-4">
                    <label> Residuo anni </label><br>
                    <div id="number-range9"></div>
                    <div id="basic_slider9"></div>
                </div>

            </div><br>

            <p>Facendo i conti l'impegno totale è di:</p>

            <div style="margin-top: 10px;" class="row">
                
                <div class="col-md-4">
                  <div class="form-group{{ $errors->has('monthly_rate') ? ' has-error' : '' }}">
                      <span> Rate Mese</span> <input name="monthly_rate"  type="text" > 
                  </div>
                  @if ($errors->has('monthly_rate'))
                    <span class="help-block">
                        <strong>{{ $errors->first('monthly_rate') }}</strong>
                    </span>
                  @endif
                  </div>
                <div class="col-md-4">
                    <div class="form-group{{ $errors->has('residual_amount') ? ' has-error' : '' }}">
                      <span> Importo Residuo</span> <input name="residual_amount" class="" type="text">
                      @if ($errors->has('residual_amount'))
                    <span class="help-block">
                        <strong>{{ $errors->first('residual_amount') }}</strong>
                    </span>
                  @endif 
                    </div>
            </div>
                <div class="col-md-4">
                    <div class="form-group{{ $errors->has('residual_years') ? ' has-error' : '' }}">
                     <span> Residuo Anni</span> <input name="residual_years" class="" type="text"> 
                 @if ($errors->has('residual_years'))
                    <span class="help-block">
                        <strong>{{ $errors->first('residual_years') }}</strong>
                    </span>
                  @endif
                 </div>
                </div>
            </div>


<div class="col-md-8">
                        
                    </div>
                    <div class="col-md-8">
        <a href="{{URL::previous()}}"type="submit" style="height:40px; width:140px; background-color:#2f4f4f"  class="btn btn-primary block full-width m-b">Indietro</a>
        </div>

                    <div class="col-md-4">
        <button type="submit" onclick="submitClient5(event)" style="height:40px; width:140px" class="btn btn-primary block full-width m-b">Avanti</button>
         </div>
    </form>
  </div>

</div>

<hr style="min-width:85%; background-color:#f09300!important; height:6px;"/>


</div>
</div>
<script type="text/javascript" src="{!! asset('js/jquery-3.1.1.min.js') !!}"></script>
<script type="text/javascript" src="{!! asset('home/ion.rangeSlider/js/ion.rangeSlider.min.js') !!}"></script>
<script type="text/javascript" src="{!! asset('home/noUiSlider.11.1.0/nouislider.min.js') !!}"></script>
<script type="text/javascript" src="{!! asset('home/jQuery-Knob/dist/jquery.knob.min.js') !!}"></script>
<script>

    var basic_slider = document.getElementById('basic_slider');

     noUiSlider.create(basic_slider, {

         start: 0,
         step:50,
         behaviour: 'tap',
         connect: 'upper',
         range: {
             'min':  0,
             'max':  3000

         

         }
     });

     var dateValues = [
         document.getElementById('number-range'),
     ];

     basic_slider.noUiSlider.on('update', function( values, handle ) {
        dateValues[handle].innerHTML = "€ " + values;
     });


     var basic_slider2 = document.getElementById('basic_slider2');

     noUiSlider.create(basic_slider2, {

         start: 0,
         step:50,
         behaviour: 'tap',
         connect: 'upper',
         range: {
             'min':  0,
             'max':  3000

         
         }
     });

     var dateValues2 = [
         document.getElementById('number-range2'),
     ];

     basic_slider2.noUiSlider.on('update', function( values, handle ) {
         dateValues2[handle].innerHTML = "€ " + values;
     });


     var basic_slider3 = document.getElementById('basic_slider3');

     noUiSlider.create(basic_slider3, {

         start: 0,
         step:50,
         behaviour: 'tap',
         connect: 'upper',
         range: {
             'min':  0,
             'max':  3000

         
        }
     });

     var dateValues3 = [
         document.getElementById('number-range3'),
     ];

     basic_slider3.noUiSlider.on('update', function( values, handle ) {
        dateValues3[handle].innerHTML = "€ " + values;
     });
    $(function() {
        $(".dial").knob({
          'format' : function (value) {
             return value + ' %';
          }});
    });

    var basic_slider4 = document.getElementById('basic_slider4');

     noUiSlider.create(basic_slider4, {

         start: 0,
         step:1000,
         behaviour: 'tap',
         connect: 'upper',
         range: {
             'min':  0,
             'max':  300000
         
        }
     });
    var dateValues4 = [
         document.getElementById('number-range4'),
     ];

     basic_slider4.noUiSlider.on('update', function( values, handle ) {
        dateValues4[handle].innerHTML = "€ " + values;
     });
    $(function() {
        $(".dial").knob({
          'format' : function (value) {
             return value + ' %';
          }});
    });

    var basic_slider5 = document.getElementById('basic_slider5');

     noUiSlider.create(basic_slider5, {

         start: 10,
         step:1000,
         behaviour: 'tap',
         connect: 'upper',
         range: {
             'min':  0,
             'max':  300000

        

        }
     });
    var dateValues5= [
         document.getElementById('number-range5'),
     ];

     basic_slider5.noUiSlider.on('update', function( values, handle ) {
        dateValues5[handle].innerHTML = "€ " + values;
     });
    $(function() {
        $(".dial").knob({
          'format' : function (value) {
             return value + ' %';
          }});
    });

    var basic_slider6 = document.getElementById('basic_slider6');

     noUiSlider.create(basic_slider6, {

         start: 0,
         step:1000,
         behaviour: 'tap',
         connect: 'upper',
         range: {
             'min':  0,
             'max':  300000

         

        }
     });
    var dateValues6= [
         document.getElementById('number-range6'),
     ];

     basic_slider6.noUiSlider.on('update', function( values, handle ) {
        dateValues6[handle].innerHTML = "€ " + values;
     });
    $(function() {
        $(".dial").knob({
          'format' : function (value) {
             return value + ' %';
          }});
    });

    var basic_slider7 = document.getElementById('basic_slider7');

     noUiSlider.create(basic_slider7, {

         start: 0,
          step:1,
         behaviour: 'tap',
         connect: 'upper',
         range: {
             'min':  0,
             'max':  30

         

        }
     });
    var dateValues7= [
         document.getElementById('number-range7'),
     ];

     basic_slider7.noUiSlider.on('update', function( values, handle ) {

        dateValues7[handle].innerHTML = " " + values;

        dateValues7[handle].innerHTML = "€ " + values;

     });
    $(function() {
        $(".dial").knob({
          'format' : function (value) {
             return value + ' %';
          }});
    });

    var basic_slider8 = document.getElementById('basic_slider8');

     noUiSlider.create(basic_slider8, {

         start: 0,
          step:1,
         behaviour: 'tap',
         connect: 'upper',
         range: {
             'min':  0,
             'max':  30

         

        }
     });
    var dateValues8= [
         document.getElementById('number-range8'),
     ];

     basic_slider8.noUiSlider.on('update', function( values, handle ) {

        dateValues8[handle].innerHTML = " " + values;

        dateValues8[handle].innerHTML = "€ " + values;

     });
    $(function() {
        $(".dial").knob({
          'format' : function (value) {
             return value + ' %';
          }});
    });

    var basic_slider9 = document.getElementById('basic_slider9');

     noUiSlider.create(basic_slider9, {

         start: 0,
         behaviour: 'tap',
         connect: 'upper',
         range: {
             'min':  0,
             'max':  30

         
        }
     });
    var dateValues9= [
         document.getElementById('number-range9'),
     ];

     basic_slider9.noUiSlider.on('update', function( values, handle ) {

        dateValues9[handle].innerHTML = " " + values;

        dateValues9[handle].innerHTML = "€ " + values;

     });
    $(function() {
        $(".dial").knob({
          'format' : function (value) {
             return value + ' %';
          }});
    });


    function submitClient5(e)
    {
        e.preventDefault();
        let mutual1 = $('select[name=mutual1]').val();
        let mutual2 = $('select[name=mutual2]').val();
        let mutual3 = $('select[name=mutual3]').val();
        
        let mutual1_monthly_payment = document.getElementById('number-range').innerHTML;
        let mutual2_monthly_payment = document.getElementById('number-range2').innerHTML;
        let mutual3_monthly_payment = document.getElementById('number-range3').innerHTML;
        let mutual1_remaining_amount = document.getElementById('number-range4').innerHTML;
        let mutual2_remaining_amount = document.getElementById('number-range5').innerHTML;
        let mutual3_remaining_amount = document.getElementById('number-range6').innerHTML;
        let mutual1_residual_years = document.getElementById('number-range7').innerHTML;
        let mutual2_residual_years = document.getElementById('number-range8').innerHTML;
        let mutual3_residual_years = document.getElementById('number-range9').innerHTML;
        
        let monthly_rate = $('input[name=monthly_rate]').val();
        let residual_amount = $('input[name=residual_amount]').val();
        let residual_years = $('input[name=residual_years]').val();

        let clientInfoId = "{{ Session::get('clientinfoid') }}"
        let url = "{{ route('postClientInfoPage5') }}";

        let formData = {
            'client_Infos_Id': clientInfoId,
            'mutual1': mutual1,
            'mutual2': mutual2,
            'mutual3': mutual3,
            'mutual1_monthly_payment': mutual1_monthly_payment,
            'mutual2_monthly_payment': mutual2_monthly_payment,
            'mutual3_monthly_payment': mutual3_monthly_payment,
            'mutual1_remaining_amount': mutual1_remaining_amount,
            'mutual2_remaining_amount' : mutual2_remaining_amount,
            'mutual3_remaining_amount': mutual3_remaining_amount,
            'mutual1_residual_years': mutual1_residual_years,
            'mutual2_residual_years': mutual2_residual_years,
            'mutual3_residual_years': mutual3_residual_years,

            'monthly_rate': monthly_rate,
            'residual_amount': residual_amount,
            'residual_years': residual_years,
        };
        console.log(formData);
        $.ajax({
            type: 'post',
            url: url,
            data: formData,
            dataType: 'json',
            success: function(res){
              console.log(res.message)
               if (res.message == "succesfully created") {
                 alert("Success! Your data has been successfully added to the database");
                 window.location = "{{ route('clientinfopage6') }}";
               } else {
                   alert("Failure! something went wrong, please try again later");
               }
            },
            error: function(data){
              var errors = data.responseJSON;
              console.log(errors.errors);
              var target = errors.errors;
              let allErrors = "";
              for (var k in target){
                if (target.hasOwnProperty(k)) {
                    allErrors += target[k] + "\n";
                }
              }
              alert(allErrors);
          }
        });
    }

</script>
</body>
</html>