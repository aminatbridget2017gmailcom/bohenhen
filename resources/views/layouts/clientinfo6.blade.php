    <!DOCTYPE html>
    <html>
      <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Protezione Famiglia</title>
        <link  href="{!! asset('home/css/bootstrap.min.css') !!}" rel="stylesheet">
        <link rel="stylesheet" href="{!! asset('home/css/font-awesome.min.css') !!}">
        <link rel="stylesheet" href="{!! asset('home/css/animate.css') !!}">
        <link rel="stylesheet" href="{!! asset('home/css/overwrite.css') !!}">
        <link href="{!! asset('home/css/animate.min.css') !!}" rel="stylesheet"> 
        <link href="{!! asset('home/css/style.css') !!}" rel="stylesheet"> 
        <link href="{!! asset('css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css') !!}"  rel="stylesheet">

         <link href ='http://fonts.googleapis.com/css?family=Roboto'rel='stylesheet'type='text/css'

  
        <header id="header">
        <nav class="navbar navbar-fixed-top" role="banner">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <a class="navbar-brand" href="/"><img src="{!! asset ('img/fep3Picture1.png')!!}" align = "right" height="100" width="160"/></a>
                </div>
 
                 <div class="navbar-image">
                            
            
                        <div class="col-md-8">
                        <div class="col-md-12">
                        <h1 class = "text-center" style= "color: #ff7302;" style ="font-size:320px;"> Protezione Famiglia<br>
                          </h1>

                         </div>
                          </div>
                          </div> 
                <div class="navbar-image">
                 <div class="col-md-2">
                 <a class="navbar-brand" href="/"><img src="{!! asset('img/logo_for_bridget.png') !!}"  align ="left" alt=""/></a>
                 
                   <!-- <img src="img/fep3Picture1.png" height="100" width="200" align ="right">-->
                </div>
                </div>         
                <!--<div class="collapse navbar-collapse navbar-right">

                    <a class="navbar-brand" href="/"><img src="{!! asset('img/logo_for_bridget.png') !!}" alt=""/></a>
                </div>              
                <div class="collapse navbar-collapse navbar-right">

                    <ul class="nav navbar-nav">
                        
                        @if (! Auth::check())
                        <li><a href="{{ route('register') }}">Registrati</a></li>
                        <li><a href="{{ route('login') }}">Login</a></li>  
                        @else
                        <li><a>Welcome {{ Auth::user()->name }}</a></li>
                        <li><a href="{{ route('signout') }}">Esci</a></li>  
                        @endif                
                    </ul>

                </div>-->

                </div>

            </div><!--/.container-->
        </nav><!--/nav-->       
    </header><!--/header--> 
    

</head>
    <body>

        <div style="margin-top: 25px;" id="feature">
            <div class="container">

<div class="row">
    

    <hr style="min-width:85%; background-color:#2f4f4f !important; height:6px;"/>

   <p>
        @if (session('clientinfopage5'))
            <div class="alert alert-success">
                {{ session('clientinfopage5') }}!
            </div>
        @endif
    </p>
    <p>
        @if (session('clientinfopage6'))
            <div class="alert alert-danger">
                {{ session('clientinfopage6') }}!
            </div>
        @endif
    </p>

  <div class="col-xs-12 col-md-6">
   <p class="text-muted"><small>PASSAGGIO 6 DI 7 </small></p>
    <p>Le tue assicurazioni attuali</p>
    <div class="navbar-image">
      <img src="{!! asset('img/Picturepage7.png') !!}"  align ="left" height="400" width="500">   
    </div>

    <!-- <div class="row">
        <p><b>Perche ti facciamo queste domande?</b></p>
        <p>
            La maggior parte dellepersonal utilizza i proppiredditi
        </p>
    </div> -->
  </div>
  

  <h3>Per ultimo ma non per questo meno importante, è conoscere quale corpertura assicurativa hai già attivato. Ti prego di includere solo le tue coperture personali (non quelle del tuo coniuge o partner).</h3>

 


  <div class="col-xs-6 col-md-6">
    <!-- <p>Qual e il tuo stato civile?</p> -->
    <form class="form-inline" role="form" method="POST" action="{{  route('postClientInfoPage6') }}">
        {{ csrf_field() }}
        <div class="for-control">
        <div class="col-md-12">
        <label>Copertura sanitaria </label> 
        <br>
        <div class="form-group{{ $errors->has('health_coverage') ? ' has-error' : '' }}">
            <span>Attiva</span>&nbsp;&nbsp;<input  type="radio" name="health_coverage" value="male">&nbsp;&nbsp; 
            
            <span>Non ce l'ho</span>&nbsp;&nbsp;<input class="" type="radio" name="health_coverage" value="I do not have it">&nbsp;&nbsp; 
            
            <span>Non sono sicuro</span>&nbsp;&nbsp;<input class="" type="radio" name="health_coverage" value="Not sure">&nbsp;&nbsp;
            
            <span>premio:&nbsp;&nbsp;</span>&nbsp;<input class=""  type="text" name="health_coverage" value="€"> &nbsp;

            @if ($errors->has('health_coverage'))
                <span class="help-block">
                    <strong>{{ $errors->first('health_coverage') }}</strong>
                </span>
            @endif
        </div>
        </div>
         </div>
        <div class="for-control">
        <div class="col-md-12">
        <br>
            
        
        <label>TCM (caso morte) </label><br>
        <div class="form-group{{ $errors->has('TSM') ? ' has-error' : '' }}">
        <span>Attiva</span>&nbsp;&nbsp;<input  type="radio" name="gender56" value="male">&nbsp;&nbsp; 
        <span>Non ce l'ho</span>&nbsp;&nbsp;<input class="" type="radio" name="TSM" value="I do not have it">&nbsp;&nbsp;
        <span>Non sono sicuro</span>&nbsp;&nbsp;<input class="" type="radio" name="TSM" value="Not sure">&nbsp;&nbsp;

        <span>premio:&nbsp;&nbsp;</span>&nbsp;<input class="" name="TSM"  type="text" value="€"> &nbsp;
        @if ($errors->has('TSM'))
            <span class="help-block">
                <strong>{{ $errors->first('TSM') }}</strong>
            </span>
        @endif

        </div>
        </div>
        
       
        <div class="for-control">
        <div class="col-md-12">
            <br>
            
        <label>Invalidità permanente da infortunio </label><br>
        <div class="form-group{{ $errors->has('disability_due_to_accident') ? ' has-error' : '' }}">
        <span>Attiva</span>&nbsp;&nbsp;<input  type="radio" name="disability_due_to_accident" value="Active">&nbsp;&nbsp;
        <span>Non ce l'ho</span>&nbsp;&nbsp;<input class="" type="radio" name="disability_due_to_accident" value="I do not have it">&nbsp;&nbsp;
        <span>Non sono sicuro</span>&nbsp;&nbsp;<input class="" type="radio" name="disability_due_to_accident" value="Not sure">&nbsp;&nbsp;

        <span>premio:&nbsp;&nbsp;</span>&nbsp;<input class="" name="disability_due_to_accident" type="text" value="€"> &nbsp;
        @if ($errors->has('disability_due_to_accident'))
            <span class="help-block">
                <strong>{{ $errors->first('disability_due_to_accident') }}</strong>
            </span>
        @endif
        </div>
        </div>
        </div>
        <div class="for-control">
        <div class="col-md-12">
            <br>
        <label>Invalidità permanente da malattia </label><br>
        <div class="form-group{{ $errors->has('disability_due_to_diseases') ? ' has-error' : '' }}">
        <span>Attiva</span>&nbsp;&nbsp;<input  type="radio" name="disability_due_to_diseases" value="Active">&nbsp;&nbsp; 
        <span>Non ce l'ho</span>&nbsp;&nbsp;<input class="" type="radio" name="disability_due_to_diseases" value="I do not have">&nbsp;&nbsp;
        <span>Non sono sicuro</span>&nbsp;&nbsp;<input class="" type="radio" name="disability_due_to_diseases" value="Not sure">&nbsp;&nbsp;

        <span>premio:&nbsp;&nbsp;</span>&nbsp;<input class="" name="disability_due_to_diseases"  type="text" value="€"> &nbsp;
        @if ($errors->has('disability_due_to_diseases'))
            <span class="help-block">
                <strong>{{ $errors->first('disability_due_to_diseases') }}</strong>
            </span>
        @endif
        </div>

        </div>
        </div>

        <div class="for-control">
        <div class="col-md-12">
            <br>
         <label>Rendita vitalizia malattia</label><br>
         <div class="form-group{{ $errors->has('injury_life_anuity') ? ' has-error' : '' }}">
        <span>Attiva</span>&nbsp;&nbsp;<input  type="radio" name="injury_life_anuity" value="Active">&nbsp;&nbsp; 
        <span>Non ce l'ho</span>&nbsp;&nbsp;<input  type="radio" name="injury_life_anuity" value="I don not have it">&nbsp;&nbsp;
        <span>Non sono sicuro</span>&nbsp;&nbsp;<input  type="radio" name="injury_life_anuity" value="Not sure">&nbsp;&nbsp;

        <span>premio:&nbsp;&nbsp;</span>&nbsp;<input class="" name="injury_life_anuity" type="text" value="€"> &nbsp;
        @if ($errors->has('injury_life_anuity'))
            <span class="help-block">
                <strong>{{ $errors->first('injury_life_anuity') }}</strong>
            </span>
        @endif
        </div>
        </div>

        <div class="for-control">
        <div class="col-md-12">
            <br>
        <label>Rendita vitalizia infortunio</label><br>
        <div class="form-group{{ $errors->has('sickeness_life_anuity') ? ' has-error' : '' }}">
        <span>Attiva</span>&nbsp;&nbsp;<input  type="radio" name="sickeness_life_anuity" value="Active">&nbsp;&nbsp;
        <span>Non ce l'ho</span>&nbsp;&nbsp;<input class="" type="radio" name="sickeness_life_anuity" value="I do not have it">&nbsp;&nbsp;
        <span>Non sono sicuro</span> &nbsp;&nbsp;<input class="" type="radio" name="sickeness_life_anuity" value="Not sure">&nbsp;&nbsp;

        <span>premio:&nbsp;&nbsp;</span>&nbsp;<input class=""  type="text" value="€"> &nbsp;
        @if ($errors->has('sickeness_life_anuity'))
            <span class="help-block">
                <strong>{{ $errors->first('sickeness_life_anuity') }}</strong>
            </span>
        @endif
        </div>

        </div>
        </div>




        <div class="col-md-8">
                        
                    </div>
                    
<div class="col-md-8">
        <a href="{{URL::previous()}}"type="submit" style="height:40px; width:140px; background-color:#2f4f4f"  class="btn btn-primary block full-width m-b">Indietro</a>
        </div>
                    <div class="col-md-4">
        
       <button type="submit" style="height:40px; width:140px" class="btn btn-primary block full-width m-b">Avanti</button>
         </div>
    </form>
  </div>

  <hr style="min-width:85%; background-color:#f09300!important; height:6px;"/>
</div>


  
</div>


</div>
</div>

</body>
</html>