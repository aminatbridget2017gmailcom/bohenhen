    <!DOCTYPE html>
    <html>
      <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Protezione Famiglia</title>
        <link  href="{!! asset('home/css/bootstrap.min.css') !!}" rel="stylesheet">
        <link  href="{!! asset('home/noUiSlider.11.1.0/nouislider.min.css') !!}" rel="stylesheet">
        {{-- <link  href="{!! asset('home/jQuery-Knob/nouislider.min.css') !!}" rel="stylesheet"> --}}

        <link rel="stylesheet" href="{!! asset('home/css/font-awesome.min.css') !!}">
        <link rel="stylesheet" href="{!! asset('home/css/animate.css') !!}">
        <link rel="stylesheet" href="{!! asset('home/css/overwrite.css') !!}">
        <link href="{!! asset('home/css/animate.min.css') !!}" rel="stylesheet"> 
        <link href="{!! asset('home/css/style.css') !!}" rel="stylesheet"> 
        <link href="{!! asset('css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css') !!}"  rel="stylesheet">
  
        <header id="header">
        <nav class="navbar navbar-fixed-top" role="banner">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/"><img src="{!! asset ('img/fep3Picture1.png')!!}" align = "right" height="100" width="160"/></a>
                </div>
 
                 <div class="navbar-image">
                            
            
                        <div class="col-md-8">
                        <div class="col-md-12">
                        <h1 class = "text-center" style= "color: #ff7302;" style ="font-size:320px;"> Protezione Famiglia<br>
                          </h1>

                         </div>
                          </div>
                          </div> 
                <div class="navbar-image">
                 <div class="col-md-2">
                 <a class="navbar-brand" href="/"><img src="{!! asset('img/logo_for_bridget.png') !!}"  align ="left" alt=""/></a>
                 
                   <!-- <img src="img/fep3Picture1.png" height="100" width="200" align ="right">-->
                </div>
                </div>
                <!--<div class="collapse navbar-collapse navbar-right">
                    <ul class="nav navbar-nav">
                        
                        @if (! Auth::check())
                        <li><a href="{{ route('register') }}">Registrati</a></li>
                        <li><a href="{{ route('login') }}">Login</a></li>  
                        @else
                        <li><a>Welcome {{ Auth::user()->name }}</a></li>
                        <li><a href="{{ route('signout') }}">Esci</a></li>  
                        @endif                
                    </ul>
                </div>-->
            </div><!--/.container-->
        </nav><!--/nav-->       
    </header><!--/header--> 
    

</head>
    <body>

        <div style="margin-top: 25px;" id="feature">
            <div class="container">

<div class="row">
    <hr style="min-width:85%; background-color:#2f4f4f !important; height:6px;"/>
    <p>
        @if (session('clientinfopage3'))
            <div class="alert alert-success">
                {{ session('clientinfopage3') }}!
            </div>
        @endif
    </p>
    <p>
        @if (session('clientinfopage4'))
            <div class="alert alert-danger">
                {{ session('clientinfopage4') }}!
            </div>
        @endif
    </p>
  <div class="col-xs-12 col-md-6">
    <p class="text-muted"><small>PASSAGGIO 4 DI 7 </small></p>
     

    <p><b>Cerchiamo di conoscere la tua <br>
        situazione finanziaria</b></p>
    <div class="navbar-image">
        <br>
      <br>
      <br>

      <br>
      <img src="{!! asset('img/Picturepage4.png') !!}" height = "320" width= "500" align ="left"> <br> 
      
      <br> 
        </div>
    
        <br>
        
  </div>

  <div class="col-xs-6 col-md-6">
    <form class="form-inline" role="form" method="POST" >
        {{ csrf_field() }}
        <div class="row">
            <div class="col-lg-12"><p class="font-bold"><b><br>Quale è il tuo reditto netto annuo? (puoi inserire cifre approssimative)<b></p></div>
            <div class="col-lg-8">
                <div id="basic_slider">
                </div>
            </div>
            <div class="col-lg-4" id="number-range">
                
            </div>
        </div>
       

       <div class="row">
             <div class="col-lg-12"><p class="font-bold"><b><br>Quale è quello del coniuge?<b></p></div>
            <div class="col-lg-8">
                <div id="basic_slider2">
                </div>
            </div>
            <div class="col-lg-4" id="number-range2">
                
            </div>
        </div>


        <div class="row">
             <div class="col-lg-12"><p class="font-bold"><b><br>Altro reddito della famiglia?<b></p></div>
            <div class="col-lg-8">
                <div id="basic_slider3">
                </div>
            </div>
            <div class="col-lg-4" id="number-range3">
                
            </div>
        </div>

        <div class="row">

            <div class="col-lg-12"><p class="font-bold"><b><br>Spese medie mensili famiglia?<b></p></div><br>
            <div class="col-lg-0" id="value-range1">
                
            </div>
            <div class="col-lg-8">
                <div id="range_slider">
                </div>
            </div>
            <div class="col-lg-4" id="value-range2">
                
            </div>
        </div>
        


        <br>

<div class="row">

<div class="form-group{{ $errors->has('shortTermAsset') ? ' has-error' : '' }}">
<label for="cognome" class="col-lg-7">Attività finanzarie a breve termine in €</label>

<div class="col-md-5">
<input id="cognome" type="text" class="form-control" name="shortTermAsset" value="{{ old('shortTermAsset') }}">

@if ($errors->has('shortTermAsset'))
<span class="help-block">
<strong>{{ $errors->first('shortTermAsset') }}</strong>
</span>
@endif
</div>
</div>


</div>
<br>


<div class="row">

<div class="form-group{{ $errors->has('mediumTermAsset') ? ' has-error' : '' }}">
<label for="cognome" class="col-lg-7">Attività finanzarie a medio termine in €</label>

<div class="col-md-5">
<input id="cognome" type="text" class="form-control" name="mediumTermAsset" value="{{ old('mediumTermAsset') }}">

@if ($errors->has('mediumTermAsset'))
<span class="help-block">
<strong>{{ $errors->first('mediumTermAsset') }}</strong>
</span>
@endif
</div>
</div>
</div>
<br>
<div class="row">

<div class="form-group{{ $errors->has('longTimeAsset') ? ' has-error' : '' }}">
<label for="cognome" class="col-md-7">Attività finanzarie a lungo termine in €</label>

<div class="col-md-5">
<input id="cognome" type="text" class="form-control" name="longTimeAsset" value="{{ old('longTimeAsset') }}">

@if ($errors->has('longTimeAsset'))
<span class="help-block">
<strong>{{ $errors->first('longTimeAsset') }}</strong>
</span>
@endif
</div>
</div>

</div>
<br>
<div class="row">
<div class="form-group{{ $errors->has('otherIncomeSupport') ? ' has-error' : '' }}">
<label for="cognome" class="col-md-7">Altre fonti di sostengno finanzario in €</label>

<div class="col-md-5">
<input id="cognome" type="text" class="form-control" name="otherIncomeSupport" value="{{ old('otherIncomeSupport') }}">

@if ($errors->has('otherIncomeSupport'))
<span class="help-block">
<strong>{{ $errors->first('otherIncomeSupport') }}</strong>
</span>
@endif
</div>
</div>
</div>
<br>

<div class="row">

<div class="form-group{{ $errors->has('supportSomeoneFinancially') ? ' has-error' : '' }}">
<label for="cognome" class="col-md-7">Sostieni finanziariamente qualcuno (€)?</label>

<div class="col-md-5">
<input id="cognome" type="text" class="form-control" name="supportSomeoneFinancially" value="{{ old('supportSomeoneFinancially') }}">

@if ($errors->has('supportSomeoneFinancially'))
<span class="help-block">
<strong>{{ $errors->first('supportSomeoneFinancially') }}</strong>
</span>
@endif
</div>
</div>
</div>


        
        <div class="row">
             <div class="col-lg-12"><p class="font-bold">
                <br>
                <b>Rapporto spese/reddito famiglia</b>&nbsp;</p></div>
            <div class="col-lg-8">
                <input type="text" value="70" name="familyExpensesIncomeRatio" class="dial" data-min = "1" data-max="100" data-fgColor="#1AB394" data-width="80" data-height="80" placeholder=% />
            </div>
            {{-- <div class="col-lg-4" id="number-range2">
                
            </div> --}}
        </div>

        <div class="row">
             <div class="col-lg-12"><p class="font-bold"><b>Rapporto spese/reddito in assenza reddito capofamiglia? </b>&nbsp;</p></div>
            <div class="col-lg-8">
                <input type="text" value="70" name="incomeWithoutBreadWinnerSupport" class="dial" data-min = "1" data-max="100" data-fgColor="#1AB394" data-width="80" data-height="80" placeholder=% />
            </div>
            {{-- <div class="col-lg-4" id="number-range2">
                
            </div> --}}
        </div>

        <div class="row">
             <div class="col-lg-12"><p class="font-bold"><b>Rapporto spese/reddito in assenza coniuge </b>&nbsp;</p></div>
            <div class="col-lg-8">
                <input type="text" value="70" name="incomeWithoutSpouse" class="dial" data-min = "1" data-max="100" data-fgColor="#1AB394" data-width="80" data-height="80" placeholder=% />
            </div>
            {{-- <div class="col-lg-4" id="number-range3">
                
            </div> --}}
        </div>

       
    </form>
  </div>
  

</div>


<div clas ="row">
<div class="col-xs-12 col-md-6">
<img src="{!! asset('img/fepPicture1.png') !!}" height="100" width= "400" align ="left">  
</div>
<div class="col-md-4">                     
</div>
<div class="col-md-2">
  <button type="submit" onclick="submitClient4()" class="btn btn-primary block full-width m-b">Avanti</button>
  <br>
</div>
</div>
<br>
<br>
<br><br>
<div class="row">
<hr style="min-width:85%; background-color:#f09300 !important; height:6px;"/>
</div>
</div>

</div>
<script type="text/javascript" src="{!! asset('js/jquery-3.1.1.min.js') !!}"></script>
<script type="text/javascript" src="{!! asset('home/noUiSlider.11.1.0/nouislider.min.js') !!}"></script>
<script type="text/javascript" src="{!! asset('home/jQuery-Knob/dist/jquery.knob.min.js') !!}"></script>
<script>
 

 
     var basic_slider = document.getElementById('basic_slider');
     
     noUiSlider.create(basic_slider, {
         start: 1000,
         behaviour: 'tap',
         connect: 'upper',
         range: {
             'min':  1000,
             'max':  400000
         }
         
     });
     var dateValues = [
         document.getElementById('number-range'),
     ];

     basic_slider.noUiSlider.on('update', function( values, handle ) {
        dateValues[handle].innerHTML = "€ " + values;
     });



     var basic_slider2 = document.getElementById('basic_slider2');

     noUiSlider.create(basic_slider2, {
         start: 1000,
         behaviour: 'tap',
         connect: 'upper',
         range: {
             'min':  1000,
             'max':  400000
         }
     });

     var dateValues2 = [
         document.getElementById('number-range2'),
     ];

     basic_slider2.noUiSlider.on('update', function( values, handle ) {
         dateValues2[handle].innerHTML = "€ " + values;
     });


     var basic_slider3 = document.getElementById('basic_slider3');

     noUiSlider.create(basic_slider3, {
         start: 1000,
         behaviour: 'tap',
         connect: 'upper',
         range: {
             'min':  1000,
             'max':  400000
        }
     });

     var dateValues3 = [
         document.getElementById('number-range3'),
     ];

     basic_slider3.noUiSlider.on('update', function( values, handle ) {
        dateValues3[handle].innerHTML = "€ " + values;
     });
    $(function() {
        $(".dial").knob({
          'format' : function (value) {
             return value + ' %';
          }});
    });

    var range_slider = document.getElementById('range_slider');

    noUiSlider.create(range_slider, {
        start: [ 0, 30000 ],
        behaviour: 'drag',
        connect: true,
        range: {
            'min':  0,
            'max':  30000
        }
    });

    var nodes = [
        document.getElementById('value-range1'), // 0
        document.getElementById('value-range2'), 
        document.getElementById('value-range3')  // 1
    ];

    range_slider.noUiSlider.on('update', function ( values, handle, unencoded, isTap, positions ) {
        nodes[handle].innerHTML = "€ " + values[handle];
    });

    function submitClient4()
    {
        let anualNet = document.getElementById('number-range').innerHTML;
        let spouseAnualNet = document.getElementById('number-range2').innerHTML;
        let familyIncome = document.getElementById('number-range3').innerHTML;
        let monthlyFamilyExpenses = document.getElementById('value-range1').innerHTML/document.getElementById('value-range2').innerHTML;
        let shortTermAsset = $('input[name=shortTermAsset]').val();
        let mediumTermAsset = $('input[name=mediumTermAsset]').val();
        let longTimeAsset = $('input[name=longTimeAsset]').val();
        let otherIncomeSupport = $('input[name=otherIncomeSupport]').val();
        let supportSomeoneFinancially = $('input[name=supportSomeoneFinancially]').val();
        let familyExpensesIncomeRatio = $('input[name=familyExpensesIncomeRatio]').val();
        let incomeWithoutBreadWinnerSupport = $('input[name=incomeWithoutBreadWinnerSupport]').val();
        let incomeWithoutSpouse = $('input[name=incomeWithoutSpouse]').val();
        let clientInfoId = "{{ Session::get('clientinfoid') }}"
        let url = "{{ route('postClientInfoPage4') }}";

        let formData = {
            'client_Infos_Id': clientInfoId,
            'anual_net': anualNet,
            'spouse_anual_net': spouseAnualNet,
            'family_income': familyIncome,
            'monthly_family_expenses': monthlyFamilyExpenses,
            'short_term_asset': shortTermAsset,
            'medium_term_asset': mediumTermAsset,
            'long_term_asset': longTimeAsset,
            'other_income_support' : otherIncomeSupport,
            'support_someone_financially': supportSomeoneFinancially,
            'family_expenses_income_ratio': familyExpensesIncomeRatio,
            'income_without_bread_winner_support': incomeWithoutBreadWinnerSupport,
            'income_without_spouse': incomeWithoutSpouse,
        };
        console.log(formData);
        $.ajax({
            type: 'post',
            url: url,
            data: formData,
            dataType: 'json',
            success: function(res){
              console.log(res.message)
               if (res.message == "succesfully created") {
                 alert("Success! Your data has been successfully added to the database");
                 window.location = "{{ route('clientinfopage5') }}";
               } else {
                   alert("Failure! something went wrong, please try again later");
               }
            },
            error: function(data){
              var errors = data.responseJSON;
              console.log(errors.errors);
              var target = errors.errors;
              let allErrors = "";
              for (var k in target){
                if (target.hasOwnProperty(k)) {
                    allErrors += target[k] + "\n";
                }
              }
              alert(allErrors);
          }
        });
    }

</script>
</body>
</html>































    