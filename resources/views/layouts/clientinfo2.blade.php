
    <!DOCTYPE html>
    <html>
      <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Protezione Famiglia</title>
        <link  href="{!! asset('home/css/bootstrap.min.css') !!}" rel="stylesheet">
        <link rel="stylesheet" href="{!! asset('home/css/font-awesome.min.css') !!}">
        <link rel="stylesheet" href="{!! asset('home/css/animate.css') !!}">
        <link rel="stylesheet" href="{!! asset('home/css/overwrite.css') !!}">
        <link href="{!! asset('home/css/animate.min.css') !!}" rel="stylesheet"> 
        <link href="{!! asset('home/css/style.css') !!}" rel="stylesheet"> 
        <link href="{!! asset('css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css') !!}"  rel="stylesheet">
  
        <header id="header">
        <nav class="navbar navbar-fixed-top" role="banner">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>


                    
                        <div class="col-md-8">                    
                        <div class="navbar-image">
                   <a class="navbar-brand" href="/"><img src="{!! asset ('img/fep3Picture1.png')!!}" align = "right" height="100" width="160"/></a>
                   
                 
                </div> 
                </div> 
                </div> 
                 
                 <div class="navbar-image">
                 <div class="col-md-8">
                        <div class="col-md-12">
                        <h1 class = "text-center" style= "color: #ff7302;" style ="font-size:320px;"> Protezione Famiglia<br>
                          </h1>

                         </div>
                          </div>
                          </div> 
                
 
            
                <div class="col-md-2">
                <div class="navbar-image">
                <a class ="navbar-brand " href="/"><img src="{!! asset('img/logo_for_bridget.png')!!}" align ="right" alt=""/></a>
                   
                </div>
                        
                 </div>      

               <!-- <div class="collapse navbar-collapse navbar-right">
                    <a class="navbar-brand" href="/"><img src="{!! asset('img/logo_for_bridget.png') !!}" alt=""/></a>
                </div>              
                <div class="collapse navbar-collapse navbar-right">

                    <ul class="nav navbar-nav">
                        
                        @if (! Auth::check())
                        <li><a href="{{ route('register') }}">Registrati</a></li>
                        <li><a href="{{ route('login') }}">Login</a></li>  
                        @else
                        <li><a> Benvenuto {{ Auth::user()->name }}</a></li>
                        <li><a href="{{ route('signout') }}">Esci</a></li>  
                        @endif                
                    </ul>
<
                </div>-->
            </div><!--/.container-->
        </nav><!--/nav-->       
    </header><!--/header--> 











                </div>
            </div><!--/.container-->
        </nav><!--/nav-->       
    </header><!--/header--> 
    


</head>
    <body>

        <div style="margin-top: 25px;" id="feature">
            <div class="container">
            {{-- <div class="row"> 
        

        <p>PASSAGGIO 1 DI 5 </p><br>
         <p>Cerchiamo di capire la tua <br>
        situazione generale</p>

    
</div> --}}

<div class="row">

    <hr style="min-width:85%; background-color:#2f4f4f !important; height:6px;"/>

    <p>
        @if (session('clientinfo'))
            <div class="alert alert-success">
                {{ session('clientinfo') }}!
            </div>
        @endif
    </p>
    <p>
        @if (session('clientinfopage2'))
            <div class="alert alert-danger">
                {{ session('clientinfopage2') }}!
            </div>
        @endif
    </p>
    
  <div class="col-xs-12 col-md-6">
     <p class="text-muted"><small>PASSAGGIO 2 DI 7 </small></p>
    <p>Cerchiamo di capire la tua <br>
        situazione familiare</p>
    <div class="navbar-image">

      <img src="{!! asset('img/Picturepage2.png') !!}" height = "350" width= "500" align ="left">   

      <!--<img src="{!! asset('img/Picturepage2.png') !!}" height = "400" width= "500" align ="left">-->   

    </div>
  </div>
  <div class="col-xs-6 col-md-6">
    <form class="form-horizontal" role="form" method="POST" action="{{ route('postClientInfoPage2') }}">
        {{ csrf_field() }}
        <div class="form-group{{ $errors->has('has_children') ? ' has-error' : '' }}">
            
            <label for="figli" class="col-md-4">Hai figli?</label>

            <div class="col-md-8">
                <b>Si</b> <input type="radio"  value="yes" name= "has_children"></input>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <b>No</b> <input type="radio"  value="No" name= "has_children"></input> &nbsp;
            </div>
            @if ($errors->has('has_children'))
                <span class="help-block">
                    <strong>{{ $errors->first('has_children') }}</strong>
                </span>
            @endif
        </div>
        <div class="form-group{{ $errors->has('date_of_birth_child_1') ? ' has-error' : '' }}">
            <label for="data-di-nascita" class="col-md-4">1. Data di nascita</label>

            <div class="col-md-8">
                <input id="data-di-nascita" type="date" class="form-control" name="date_of_birth_child_1" value="{{ old('date_of_birth_child_1') }}">

                @if ($errors->has('date_of_birth_child_1'))
                    <span class="help-block">
                        <strong>{{ $errors->first('date_of_birth_child_1') }}</strong>
                    </span>
                @endif
            </div>
        </div>
      <br>
        <div class="form-group{{ $errors->has('date_of_birth_child_2') ? ' has-error' : '' }}">
            <label for="data-di-nascita-2" class="col-md-4">2. Data di nascita</label>

            <div class="col-md-8">
                <input id="data-di-nascita-2" type="date" class="form-control" name="date_of_birth_child_2" value="{{ old('date_of_birth_child_2') }}">

                @if ($errors->has('date_of_birth_child_2'))
                    <span class="help-block">
                        <strong>{{ $errors->first('date_of_birth_child_2') }}</strong>
                    </span>
                @endif
            </div>
        </div>
     <br>
        <div class="form-group{{ $errors->has('date_of_birth_child_3') ? ' has-error' : '' }}">
            <label for="data-di-nascita-3" class="col-md-4">3. Data di nascita</label>

            <div class="col-md-8">
                <input id="data-di-nascita-3" type="date" class="form-control" name="date_of_birth_child_3" value="{{ old('date_of_birth_child_3') }}">

                @if ($errors->has('date_of_birth_child_3'))
                    <span class="help-block">
                        <strong>{{ $errors->first('date_of_birth_child_3') }}</strong>
                    </span>
                @endif
            </div>
        </div>
      <br>
        <div class="form-group{{ $errors->has('children_in_future') ? ' has-error' : '' }}">
            <label for="cognome" class="col-md-4">Previsti in futuro</label>

            <div class="col-md-8">
                <b>&nbsp;&nbsp;Si&nbsp;&nbsp;</b> <input type="radio" name="children_in_future" value="yes"> </input>
                <b>&nbsp;&nbsp;No</b> <input type="radio" name="children_in_future" value="No"></input>
            </div>
            @if ($errors->has('children_in_future'))
                <span class="help-block">
                    <strong>{{ $errors->first('children_in_future') }}</strong>
                </span>
            @endif
        </div>
      <br>


      <div class="form-group{{ $errors->has('has_pet') ? ' has-error' : '' }}">
            <label for="cognome" class="col-md-4">Hai cani o gatti?</label>

            <div class="col-md-8">
                <b>&nbsp;&nbsp;Si&nbsp;&nbsp;</b> <input type="radio" name="has_pet" value="yes"> </input>
                <b>&nbsp;&nbsp;No</b> <input type="radio" name="has_pet" value="No"></input>
            </div>
            @if ($errors->has('has_pet'))
                <span class="help-block">
                    <strong>{{ $errors->first('has_pet') }}</strong>
                </span>
            @endif
        </div>
      <br>
        <div class="form-group{{ $errors->has('future_dependency') ? ' has-error' : '' }}">

            <p class="col-md-12"><b>Quanto vuoi sia determinante il tuo aiuto per la costituzione del loro futuro <br> (studio, matrimonio, casa ecc..)?</b></p>
            <div class="col-md-8">
                <input type="radio" name="future_dependency" value="Total dependency"><b>&nbsp;&nbsp;Totalmente a mio carico</b>
                
                <br>
                <input type="radio" name="future_dependency" value="partial dependency"><b>&nbsp;&nbsp;Parzialmente a mio carico</b>
                
                <br>
                <input type="radio" name="future_dependency" value="No dependency"><b>&nbsp;&nbsp;Ininfluente</b>
                @if ($errors->has('future_dependency'))
                    <span class="help-block">
                        <strong>{{ $errors->first('future_dependency') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="col-md-8">
                        
        </div>
        <div class="col-md-8">
            <a href="{{URL::previous()}}"type="submit" style="height:40px; width:140px; background-color:#2f4f4f"  class="btn btn-primary block full-width m-b">Indietro</a>
        </div>
        <div class="col-md-4">
            <button type="submit" style="height:40px; width:140px" class="btn btn-primary block full-width m-b">Avanti</button>
         </div>
    </form>
  </div>

  
</div>

<hr style="min-width:85%; background-color:#f09300 !important; height:6px;"/>



</div>
</div>
</body>
</html>