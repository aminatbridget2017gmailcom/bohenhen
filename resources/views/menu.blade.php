    <!DOCTYPE html>
    <html lang="en">
      <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Menu</title>

        <!-- Bootstrap -->
        <link href="home/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="home/css/font-awesome.min.css">
        <link rel="stylesheet" href="home/css/animate.css">
        <link rel="stylesheet" href="home/css/overwrite.css">
        <link href="home/css/animate.min.css" rel="stylesheet"> 
        <link href="home/css/style.css" rel="stylesheet">  
      </head>
      <body>    
        <header id="header">
        <nav class="navbar navbar-fixed-top" role="banner">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/"><img src="img/logo_for_bridget.png" alt=""/></a>
                </div>              
                <div class="collapse navbar-collapse navbar-right">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="/">Home</a></li>
                        @if (! Auth::check())
                        <li><a href="{{ route('register') }}">Registrati</a></li>
                        <li><a href="{{ route('login') }}">Login</a></li>  
                        @else

                        <li><a>Bevenuto {{ Auth::user()->name }}</a></li>

                        <!--<li><a>Welcome {{ Auth::user()->name }}</a></li>-->

                        <li><a href="{{ route('signout') }}">Esci</a></li>  
                        @endif                
                    </ul>
                </div>
            </div><!--/.container-->
        </nav><!--/nav-->       
    </header><!--/header--> 

 



<hr style ="border-top:1px solid #ccc; background:yellow;"></hr>
        <div id="feature">
            <div class="container">
                <div class="row">

                    <hr style="min-width:85%; background-color:#2f4f4f !important; height:6px;"/>  
                    
                    <p>
                        @if (session('clientfirstinfo'))
                            <div class="alert alert-success">
                                {{ session('clientfirstinfo') }}!
                            </div>
                        @endif
                    </p>
                    <!--<hr style="min-width:85%; background-color:#ff7302!important; height:10px;"/>-->

                    <h1 style="color: #ff7302;"> Da dove vuoi iniziare?</h1>
                    <p>I nostri Insurance Advisor possono aiutarti a scegliere da quale check-up  iniziare.
                    Puoi effettuare una semplice Spending
                    Review Assicurativa oppure un’analisi completa
                    delle necessità assicuratevi per la tua famiglia.
                    Scopri come si svolgono i check-up e
                    contattaci compilando il format dedicato.</p>
                    <div class="one-container">
                        <div class="main-image">
                            <div class="row">
                                <div class="col-xs-12 col-md-7" >
                                    <a href ="{{ route('clientinfo') }}" target ="_blank">
                                    <img src="img/sicurobuttoni.png"  style ="float:middle">
                                        </a>

                     
                                      <!--  <img src="img/Picture9.png" height = "200" width ="445" style ="float:middle"></a>
                                    <a href ="#" target ="_blank"><img src="img/Picture11.png" height="200" width="220"></a>
                                    <div style="margin-top: 1%;" class="">
                                        <a href="#" target ="_blank"><img src="img/Picture12new.png" height = "190" width= "220"></a>
                                        <a href ="#" target ="_blank"><img src="img/Picture13.png" height = "190" width= "222"> </a>
                                        <a href ="#" target ="_blank"><img src="img/Picture14.png" height = "190" width= "220"></a>
                                    </div>
                                   --> 
                                </div>

                                <div class="col-md-8">
                        
                    </div>
                    <div class="col-md-4">
                        <a href="{{URL::previous()}}"type="submit" style="height:40px; width:140px; background-color:#2f4f4f"  class="btn btn-primary block full-width m-b">Indietro</a>
                    </div>
                                
                            </div>
                        </div>
                       <hr style="min-width:85%; background-color:#ff7302!important; height:6px;"/>

                                <!--<div class="col-xs-6 col-md-5" >
                                    <div class="clearfix visible-xs-block"></div>
                                    <a style="margin-left: -25px;" href ="#" target ="_blank"><img src="img/Picture15.png" height = "383" width= "400"></a>

                                </div>
                                </div>-->
                                
                            </div>
                        </div>
                       

                    </div>
                   
                </div>

            </div>

        </div>

        
    </body>
    </html>


