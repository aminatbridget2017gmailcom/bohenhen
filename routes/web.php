<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/signout', 'Auth\LoginController@logout')->name('signout');

Route::get('/dashboard', 'HomeController@index')->name('home');
Route::get('/menu', 'MenuController@index')->name('menu');
Route::get('/clientinfo', 'ClientinfoController@index')->name('clientinfo')->middleware('auth');
Route::post('/clientinfo', 'ClientinfoController@postClientInfo')->name('postClientinfo')->middleware('auth');
Route::get('/clientinfo/page2', 'ClientinfoController@page2')->name('clientinfopage2')->middleware('auth');
Route::post('/clientinfo/page2', 'ClientinfoController@postClientInfoPage2')->name('postClientInfoPage2')->middleware('auth');
Route::get('/clientinfo/page3', 'ClientinfoController@page3')->name('clientinfopage3')->middleware('auth');
Route::post('/clientinfo/page3', 'ClientinfoController@postClientInfoPage3')->name('postClientInfoPage3')->middleware('auth');
Route::get('/clientinfo/page4', 'ClientinfoController@page4')->name('clientinfopage4')->middleware('auth');


Route::get('/clientinfo/page5', 'ClientinfoController@page5')->name('clientinfopage5')->middleware('auth');
Route::get('/clientinfo/page6', 'ClientinfoController@page6')->name('clientinfopage6')->middleware('auth');
Route::post('/clientinfo/page6', 'ClientinfoController@postClientInfoPage6')->name('postClientInfoPage6')->middleware('auth');
Route::get('/clientinfo/page7', 'ClientinfoController@page7')->name('clientinfopage7')->middleware('auth');
Route::post('/clientinfo/page7', 'ClientinfoController@postClientInfoPage7')->name('postClientInfoPage7')->middleware('auth');
Route::get('/clientfirstinfo', 'ClientfirstinfoController@index')->name('clientfirstinfo')->middleware('auth');
Route::post('/clientfirstinfo', 'ClientfirstinfoController@postClientFirstInfo')->name('postClientFirstInfo')->middleware('auth');


Route::get('/datamobili', 'DatamobiliController@index')->name('datamobili')->middleware('auth');
Route::get('/datamobili2', 'DatamobiliController@indexpage2')->name('datamobili2')->middleware('auth');
Route::get('/datamobili3', 'DatamobiliController@indexpage3')->name('datamobili3')->middleware('auth');
Route::get('/datamobili4', 'DatamobiliController@indexpage4')->name('datamobili4')->middleware('auth');

Route::get('/datamobili', 'DatamobiliController@index')->name('datamobili')->middleware('auth');

