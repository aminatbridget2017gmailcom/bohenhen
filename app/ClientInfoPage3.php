<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientInfoPage3 extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'client_infos_id', 'marital_status', 'spouse_activity', 'residential', 'has_property_for_rent'
    ];

    /**
     * Define members table relationship
     *
     * @return object
     */
    public function clientInfo()
    {
        return $this->belongsTo('App\ClientInfo');
    }
}
