<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ClientInfoPage6Request extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'health_coverage' => 'required',
            'TSM' => 'required',
            'disability_due_to_accident' => 'required',
            'disability_due_to_diseases' => 'required',
            'injury_life_anuity' => 'required',
            'sickeness_life_anuity' => 'required',
        ];
    }
}
