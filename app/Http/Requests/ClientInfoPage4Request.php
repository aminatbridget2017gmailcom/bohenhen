<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ClientInfoPage4Request extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'anual_net' => 'required',
            'spouse_anual_net' => 'required',
            'family_income' => 'required',
            'monthly_family_expenses' => 'required',
            'short_term_asset' => 'required',
            'medium_term_asset' => 'required',
            'long_term_asset' => 'required',
            'other_income_support' => 'required',
            'support_someone_financially' => 'required',
            'family_expenses_income_ratio' => 'required',
            'income_without_bread_winner_support' => 'required',
            'income_without_spouse' => 'required'
        ];
    }
}
