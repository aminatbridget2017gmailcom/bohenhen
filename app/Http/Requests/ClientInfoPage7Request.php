<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ClientInfoPage7Request extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'daily_allowance_from_gyp_hosp_conv' => 'required',
            'daily_dario' => 'required',
            'permanent_disability' => 'required',
            'active_diseases' => 'required',
            'temporary_disability' => 'required',
            'long_time_care' => 'required',
            'active_auto' => 'required',
            'active_institutional' => 'required',
            'campofamiglia' => 'required',
            'active_pets' => 'required',
            'damage_coverage' => 'required',
            'long_term_care' => 'required',
            'total_premium_anual' => 'required',
            'ideal' => 'required',
            'total_in_euro' => 'required',
        ];
    }
}
