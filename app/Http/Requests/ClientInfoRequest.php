<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ClientInfoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required',
            'surname' => 'required',
            'gender' => 'required',
            'date' => 'required',
            'place_of_birth' => 'required',
            'address' => 'required',
            'gender' => 'required',
            'work_status' => 'required',
            'business_type' => 'required',
            'employment_status' => 'required',
        ];
    }
}
