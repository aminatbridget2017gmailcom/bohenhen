<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ClientInfoPage5Request extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'mutual1' => 'required',
            'mutual2' => 'required',
            'mutual3' => 'required',
            'mutual2_monthly_payment' => 'required',
            'mutual1_monthly_payment' => 'required',
            'mutual3_monthly_payment' => 'required',
            'mutual2_remaining_amount' => 'required',
            'mutual1_remaining_amount' => 'required',
            'mutual3_remaining_amount' => 'required',
            'mutual1_residual_years' => 'required',
            'mutual2_residual_years' => 'required',
            'mutual3_residual_years' => 'required',
            'monthly_rate' => 'required',
            'residual_amount' => 'required',
            'residual_years' => 'required'
        ];
    }
}
