<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ClientFirstInfoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'fName' => 'required',
            'surname' => 'required',
            'gender' => 'required',
            'date' => 'required',
            'telephone' => 'required|regex:/(39)[0-9]{10}/|unique:client_first_infos,telephone',
            'mail' => 'required|max:255|unique:client_first_infos,mail',
            'accept' => 'required'
        ];
    }
}
