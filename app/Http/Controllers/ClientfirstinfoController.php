<?php

namespace App\Http\Controllers;

use App\ClientFirstInfo;
use Illuminate\Http\Request;
use App\Http\Requests\ClientFirstInfoRequest;

class ClientfirstinfoController extends Controller
{
    public function index()
    {
        return view('clientfirstinfo');
    }

    public function postClientFirstInfo(ClientFirstInfoRequest $request)
    {
    	$clientFirstInfo = ClientFirstInfo::create([
    		'first_name' => $request->fName,
            'surname' => $request->surname,
            'date_of_birth'    => $request->date,
            'gender' => $request->gender,
            'mail' => $request->mail,
            'telephone'   => $request->telephone,
    	]);

    	if ($clientFirstInfo) {
    	    return redirect()->route('menu')->with('clientfirstinfo', 'Suceess! Data successfully saved to the database');
    	}

    	return redirect()->back()->with('clientfirstinfo', 'Failure! Oops, Something went wrong');

    	
    }  
}
