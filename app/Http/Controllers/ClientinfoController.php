<?php

namespace App\Http\Controllers;

use Session;
use App\ClientInfo;
use App\ClientInfoPage2;
use App\ClientInfoPage3;
use App\ClientInfoPage4;
use App\ClientInfoPage5;
use App\ClientInfoPage6;
use App\ClientInfoPage7;
use Illuminate\Http\Request;
use App\Http\Requests\ClientInfoRequest;
use App\Http\Requests\ClientInfoPage2Request;
use App\Http\Requests\ClientInfoPage3Request;
use App\Http\Requests\ClientInfoPage4Request;
use App\Http\Requests\ClientInfoPage5Request;
use App\Http\Requests\ClientInfoPage6Request;
use App\Http\Requests\ClientInfoPage7Request;

class ClientinfoController extends Controller
{
    public function index()
    {
        return view('clientinfo');
    }

    public function postClientInfo(ClientInfoRequest $request)
    {
        $clientInfo = ClientInfo::create([
            'first_name' => $request->first_name,
            'surname' => $request->surname,
            'date_of_birth'    => $request->date,
            'gender' => $request->gender,
            'place_of_birth' => $request->place_of_birth,
            'address'   => $request->address,
            'work_status'   => $request->work_status,
            'employment_status'   => $request->employment_status,
            'business_type'   => $request->business_type,
        ]);

        Session::put('clientinfoid', $clientInfo->id);

        if ($clientInfo) {
            return redirect()->route('clientinfopage2')->with('clientinfo', 'Sucess! Data successfully saved to the database');
        }

        return redirect()->back()->with('clientinfo', 'Failure! Oops, Something went wrong');
    }

    public function page2()
    {
        return view('layouts.clientinfo2');
    }

    public function postClientInfoPage2(ClientInfoPage2Request $request)
    {
        $clientInfoPage2 = ClientInfoPage2::create([
            'client_infos_id' => Session::get('clientinfoid'),
            'has_children' => $request->has_children,
            'date_of_birth_child_1' => $request->date_of_birth_child_1,
            'date_of_birth_child_2' => $request->date_of_birth_child_2,
            'date_of_birth_child_3' => $request->date_of_birth_child_3,
            'children_in_future' => $request->children_in_future,
            'future_dependency' => $request->future_dependency,
            'has_pet' => $request->has_pet,
        ]);

        if ($clientInfoPage2) {
            return redirect()->route('clientinfopage3')->with('clientinfopage2', 'Sucess! Data successfully saved to the database');
        }

        return redirect()->back()->with('clientinfopage2', 'Failure! Oops, Something went wrong');
    }

    public function page3()
    {
        return view('layouts.clientinfo3');
    }

    public function postClientInfoPage3(ClientInfoPage3Request $request)
    {
        $clientInfoPage3 = ClientInfoPage3::create([
            'client_infos_id' => Session::get('clientinfoid'),
            'marital_status' => $request->marital_status,
            'spouse_activity' => $request->spouse_activity,
            'residential' => $request->residential,
            'has_property_for_rent' => $request->has_property_for_rent,
        ]);

        if ($clientInfoPage3) {
            return redirect()->route('clientinfopage4')->with('clientinfopage3', 'Sucess! Data successfully saved to the database');
        }

        return redirect()->back()->with('clientinfopage3', 'Failure! Oops, Something went wrong');
    }

    public function page4()
    {
        return view('layouts.clientinfo4');
    }

    public function postClientInfoPage4(ClientInfoPage4Request $request)
    {
        $clientInfoPage4 = ClientInfoPage4::create([
            'client_infos_id' => $request->client_Infos_Id,
            'anual_net' => $request->anual_net,
            'spouse_anual_net' => $request->spouse_anual_net,
            'family_income' => $request->family_income,
            'monthly_family_expenses' => $request->monthly_family_expenses,
            'short_term_asset' => $request->short_term_asset,
            'medium_term_asset' => $request->medium_term_asset,
            'long_term_asset' => $request->long_term_asset,
            'other_income_support' => $request->other_income_support,
            'support_someone_financially' => $request->support_someone_financially,
            'family_expenses_income_ratio' => $request->family_expenses_income_ratio,
            'income_without_bread_winner_support' => $request->income_without_bread_winner_support,
            'income_without_spouse' => $request->income_without_spouse,
        ]);

        if ($clientInfoPage4) {
            return response()->json(['message' => 'succesfully created'], 201);
        } else {
            return response()->json(['message' => 'something went wrong'], 401);
        }
    }

    public function page5()
    {
        return view('layouts.clientinfo5');
    }

    public function postClientInfoPage5(ClientInfoPage5Request $request)
    {
        $clientInfoPage5 = ClientInfoPage5::create([
            'client_infos_id' => $request->client_Infos_Id,
            'mutual1' => $request->mutual1,
            'mutual2' => $request->mutual2,
            'mutual3' => $request->mutual3,
            'mutual1_monthly_payment' => $request->mutual1_monthly_payment,
            'mutual2_monthly_payment' => $request->mutual2_monthly_payment,
            'mutual3_monthly_payment' => $request->mutual3_monthly_payment,
            'mutual1_remaining_amount' => $request->mutual1_remaining_amount,
            'mutual2_remaining_amount' => $request->mutual2_remaining_amount,
            'mutual3_remaining_amount' => $request->mutual3_remaining_amount,
            'mutual1_residual_years' => $request->mutual1_residual_years,
            'mutual2_residual_years' => $request->mutual2_residual_years,
            'mutual3_residual_years' => $request->mutual3_residual_years,
            'monthly_rate' => $request->monthly_rate,
            'residual_amount' => $request->residual_amount,
            'residual_years' => $request->residual_years,
        ]);

        if ($clientInfoPage5) {
            return response()->json(['message' => 'succesfully created'], 201);
        } else {
            return response()->json(['message' => 'something went wrong'], 401);
        }
    }

    public function page6()
    {
        return view('layouts.clientinfo6');
    }

    public function postClientInfoPage6(ClientInfoPage6Request $request)
    {
        $clientInfoPage6 = ClientInfoPage6::create([
            'client_infos_id' => Session::get('clientinfoid'),
            'health_coverage' => $request->health_coverage,
            'TSM' => $request->TSM,
            'disability_due_to_accident' => $request->disability_due_to_accident,
            'disability_due_to_diseases' => $request->disability_due_to_diseases,
            'injury_life_anuity' => $request->injury_life_anuity,
            'sickeness_life_anuity' => $request->sickeness_life_anuity,
        ]);

        if ($clientInfoPage6) {
            return redirect()->route('clientinfopage7')->with('clientinfopage6', 'Sucess! Data successfully saved to the database');
        }

        return redirect()->back()->with('clientinfopage6', 'Failure! Oops, Something went wrong');
    }

    public function page7()
    {
        return view('layouts.clientinfo7');
    }

    public function postClientInfoPage7(ClientInfoPage7Request $request)
    {
        $clientInfoPage7 = ClientInfoPage7::create([
            'client_infos_id' => Session::get('clientinfoid'),
            'daily_allowance_from_gyp_hosp_conv' => $request->daily_allowance_from_gyp_hosp_conv,
            'daily_dario' => $request->daily_dario,
            'permanent_disability' => $request->permanent_disability,
            'active_diseases' => $request->active_diseases,
            'temporary_disability' => $request->temporary_disability,
            'long_time_care' => $request->long_time_care,
            'active_auto' => $request->active_auto,
            'active_institutional' => $request->active_institutional,
            'campofamiglia' => $request->campofamiglia,
            'active_pets' => $request->active_pets,
            'damage_coverage' => $request->damage_coverage,
            'long_term_care' => $request->long_term_care,
            'total_premium_anual' => $request->total_premium_anual,
            'ideal' => $request->ideal,
            'total_in_euro' => $request->total_in_euro,
        ]);

        if ($clientInfoPage7) {
            return view('home');
        }

        return redirect()->back()->with('clientinfopage6', 'Failure! Oops, Something went wrong');
    }
}
