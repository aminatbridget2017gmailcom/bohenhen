<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientInfo extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'surname', 'date_of_birth', 'place_of_birth', 'address', 'gender', 'business_type', 'employment_status', 'work_status'
    ];

    /**
     * Define members table relationship
     *
     * @return object
     */
    public function clientInfoPage2()
    {
        return $this->hasOne('App\ClientInfoPage2');
    }

    /**
     * Define members table relationship
     *
     * @return object
     */
    public function clientInfoPage3()
    {
        return $this->hasOne('App\ClientInfoPage3');
    }

    /**
     * Define members table relationship
     *
     * @return object
     */
    public function clientInfoPage4()
    {
        return $this->hasOne('App\ClientInfoPage4');
    }

    /**
     * Define members table relationship
     *
     * @return object
     */
    public function clientInfoPage5()
    {
        return $this->hasOne('App\ClientInfoPage5');
    }

    /**
     * Define members table relationship
     *
     * @return object
     */
    public function clientInfoPage6()
    {
        return $this->hasOne('App\ClientInfoPage6');
    }

    /**
     * Define members table relationship
     *
     * @return object
     */
    public function clientInfoPage7()
    {
        return $this->hasOne('App\ClientInfoPage7');
    }
}
