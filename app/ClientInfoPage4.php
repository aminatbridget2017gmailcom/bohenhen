<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientInfoPage4 extends Model
{
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
	    'client_infos_id', 'anual_net', 'spouse_anual_net', 'family_income', 'monthly_family_expenses', 'short_term_asset', 'medium_term_asset', 'long_term_asset', 'other_income_support', 'support_someone_financially', 'family_expenses_income_ratio', 'income_without_bread_winner_support', 'income_without_spouse',
	];

    /**
     * Define members table relationship
     *
     * @return object
     */
    public function clientInfo()
    {
        return $this->belongsTo('App\ClientInfo');
    }
}
