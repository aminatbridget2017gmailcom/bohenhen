<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientInfoPage5 extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
	    'client_infos_id', 'mutual1', 'mutual2', 'mutual3', 'mutual1_monthly_payment', 'mutual2_monthly_payment', 'mutual3_monthly_payment', 'mutual1_remaining_amount', 'mutual2_remaining_amount', 'mutual3_remaining_amount', 'mutual1_residual_years', 'mutual2_residual_years', 'mutual3_residual_years', 'monthly_rate', 'residual_amount', 'residual_years',
	];

    /**
     * Define members table relationship
     *
     * @return object
     */
    public function clientInfo()
    {
        return $this->belongsTo('App\ClientInfo');
    }
}
