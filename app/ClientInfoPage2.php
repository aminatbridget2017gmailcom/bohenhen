<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientInfoPage2 extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'client_infos_id', 'has_children', 'date_of_birth_child_1', 'date_of_birth_child_2', 'date_of_birth_child_3', 'children_in_future', 'future_dependency', 'has_pet'
    ];

    /**
     * Define members table relationship
     *
     * @return object
     */
    public function clientInfo()
    {
        return $this->belongsTo('App\ClientInfo');
    }
}
