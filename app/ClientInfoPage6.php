<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientInfoPage6 extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
	    'client_infos_id', 'health_coverage', 'TSM', 'disability_due_to_accident', 'disability_due_to_diseases', 'injury_life_anuity', 'sickeness_life_anuity',
	];

    /**
     * Define members table relationship
     *
     * @return object
     */
    public function clientInfo()
    {
        return $this->belongsTo('App\ClientInfo');
    }
}
