<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientInfoPage7 extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
	    'client_infos_id', 'daily_allowance_from_gyp_hosp_conv', 'daily_dario', 'permanent_disability', 'active_diseases', 'temporary_disability', 'long_time_care', 'active_auto', 'active_institutional', 'campofamiglia', 'active_pets', 'damage_coverage', 'long_term_care', 'total_premium_anual', 'ideal', 'total_in_euro',
	];

    /**
     * Define members table relationship
     *
     * @return object
     */
    public function clientInfo()
    {
        return $this->belongsTo('App\ClientInfo');
    }
}
