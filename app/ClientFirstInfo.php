<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientFirstInfo extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'surname', 'date_of_birth', 'telephone', 'mail', 'gender'
    ];
}
