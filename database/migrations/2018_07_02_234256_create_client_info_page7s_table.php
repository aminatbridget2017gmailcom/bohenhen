<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientInfoPage7sTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_info_page7s', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('client_infos_id')->unsigned();
            $table->string('daily_allowance_from_gyp_hosp_conv');
            $table->string('daily_dario');
            $table->string('permanent_disability');
            $table->string('active_diseases');
            $table->string('temporary_disability');
            $table->string('long_time_care');
            $table->string('active_auto');
            $table->string('active_institutional');
            $table->string('campofamiglia');
            $table->string('active_pets');
            $table->string('damage_coverage');
            $table->string('long_term_care');
            $table->string('total_premium_anual');
            $table->string('ideal');
            $table->string('total_in_euro');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_info_page7s');
    }
}
