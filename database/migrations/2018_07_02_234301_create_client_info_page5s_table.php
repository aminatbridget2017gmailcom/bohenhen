<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientInfoPage5sTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_info_page5s', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('client_infos_id')->unsigned();
            $table->string('mutual1');
            $table->string('mutual2');
            $table->string('mutual3');
            $table->string('mutual1_monthly_payment');
            $table->string('mutual2_monthly_payment');
            $table->string('mutual3_monthly_payment');
            $table->string('mutual1_remaining_amount');
            $table->string('mutual2_remaining_amount');
            $table->string('mutual3_remaining_amount');
            $table->string('mutual1_residual_years');
            $table->string('mutual2_residual_years');
            $table->string('mutual3_residual_years');
            $table->string('monthly_rate');
            $table->string('residual_amount');
            $table->string('residual_years');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_info_page5s');
    }
}
