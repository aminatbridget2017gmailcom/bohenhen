<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientInfoPage3sTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_info_page3s', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('client_infos_id')->unsigned();
            $table->string('marital_status');
            $table->string('spouse_activity');
            $table->string('residential');
            $table->string('has_property_for_rent');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_info_page3s');
    }
}
