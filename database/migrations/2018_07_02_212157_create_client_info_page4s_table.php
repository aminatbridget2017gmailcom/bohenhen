<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientInfoPage4sTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_info_page4s', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('client_infos_id')->unsigned();
            $table->string('anual_net');
            $table->string('spouse_anual_net');
            $table->string('family_income');
            $table->string('monthly_family_expenses');
            $table->string('short_term_asset');
            $table->string('medium_term_asset');
            $table->string('long_term_asset');
            $table->string('other_income_support');
            $table->string('support_someone_financially');
            $table->string('family_expenses_income_ratio');
            $table->string('income_without_bread_winner_support');
            $table->string('income_without_spouse');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_info_page4s');
    }
}
