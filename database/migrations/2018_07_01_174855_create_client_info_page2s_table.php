<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientInfoPage2sTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_info_page2s', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('client_infos_id')->unsigned();
            $table->string('has_children');
            $table->string('date_of_birth_child_1')->nullable();
            $table->string('date_of_birth_child_2')->nullable();
            $table->string('date_of_birth_child_3')->nullable();
            $table->string('children_in_future');
            $table->string('has_pet');
            $table->string('future_dependency');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_info_page2s');
    }
}
